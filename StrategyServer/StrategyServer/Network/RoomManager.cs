using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StrategyServer
{
    class RoomManager
    {
        public readonly int roomId;

        private readonly int maxPlayers;
        private readonly int minPlayers;
        internal readonly Game game;

        public readonly Dictionary<int, bool> playersReady;

        public RoomManager(int _minPlayers, int _maxPlayers, int id)
        {
            maxPlayers = _maxPlayers;
            minPlayers = _minPlayers;
            roomId = id;

            game = new Game(this);
            playersReady = new Dictionary<int, bool>();
        }

        public bool PlayerJoin(int player)
        {
            if (playersReady.Count == maxPlayers)
            {
                return false;
            }

            Server.clients[player].room = this;
            playersReady.Add(player, false);
            return true;
        }

        public void PlayerDisconnect(int player)
        {
            Server.clients[player].room = null;
            playersReady.Remove(player);

            if (playersReady.Count == 0)
            {
                Server.RemoveRoom(this);
            }
        }

        public void PlayerChangeReadyState(int player)
        {
            playersReady[player] = !playersReady[player];

            bool readyToStart = true;
            if (playersReady.Count < minPlayers)
                readyToStart = false;
            else
            {
                foreach (bool isReady in playersReady.Values)
                {
                    if (!isReady)
                    {
                        readyToStart = false;
                        break;
                    }
                }
            }

            if (readyToStart)
            {
                StartGame();
            }
        }

        public void StartGame()
        {
            game.StartGame(playersReady.Keys.ToArray(), 5, 7);

            Console.WriteLine($"The game in room {roomId} has started.");
            ServerSend.GameStarted(this, game.map);
        }

        public void GameEnded()
        {
            //Reset players' ready state
            int[] clients = playersReady.Keys.ToArray();
            foreach (int client in clients)
            {
                playersReady[client] = false;
            }

            Console.WriteLine($"The game in room {roomId} has ended.");
            ServerSend.GameEnded(this);
        }

    }
}
