using System;
using System.Collections.Generic;
using System.Linq;

namespace StrategyServer
{
    class ServerSend
    {
        private static void SendTCPData(int toClient, Packet packet)
        {
            packet.WriteLength();
            Server.clients[toClient].tcp.SendData(packet);
        }

        private static void SendTCPDataToAll(IEnumerable<int> toClients, Packet packet)
        {
            packet.WriteLength();
            foreach (int toClient in toClients)
            {
                Server.clients[toClient].tcp.SendData(packet);
            }
        }

        private static void SendTCPDataToAll(Packet packet)
        {
            packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                Server.clients[i].tcp.SendData(packet);
            }
        }

        private static void SendTCPDataToAll(int exceptClient, Packet packet)
        {
            packet.WriteLength();
            for (int i = 1; i <= Server.MaxPlayers; i++)
            {
                if (i != exceptClient)
                {
                    Server.clients[i].tcp.SendData(packet);
                }
            }
        }

        #region Packets
        public static void Welcome(int toClient, string msg)
        {
            using (Packet packet = new Packet((int)ServerPackets.welcome))
            {
                packet.Write(msg);
                packet.Write(toClient);

                SendTCPData(toClient, packet);
            }
        }

        public static void RoomsState(int toClient)
        {
            using (Packet packet = new Packet((int)ServerPackets.roomsState))
            {
                //Send current rooms' state
                packet.Write(Server.rooms.Count);
                for (int i = 0; i < Server.rooms.Count; i++)
                {
                    RoomManager room = Server.rooms.Values.ElementAt(i);
                    packet.Write(room.roomId);
                    packet.Write(room.playersReady.Count);
                    // for (int j = 0; j < room.playersReady.Count; j++)
                    // {
                    //     var playerReady = room.playersReady.ElementAt(j);
                    //     packet.Write(playerReady.Key);
                    //     packet.Write(playerReady.Value);
                    // }
                }

                SendTCPData(toClient, packet);
            }
        }

        public static void RoomHosted(int toClient, int roomId)
        {
            using (Packet packet = new Packet((int)ServerPackets.roomHosted))
            {
                packet.Write(toClient);
                packet.Write(roomId);

                SendTCPData(toClient, packet);
            }
        }

        public static void RoomInfo(int toClient, int roomId)
        {
            using (Packet packet = new Packet((int)ServerPackets.roomInfo))
            {
                packet.Write(roomId);

                RoomManager room = Server.rooms[roomId];
                packet.Write(room.playersReady.Count);
                foreach (int playerId in room.playersReady.Keys)
                {
                    Client player = Server.clients[playerId];
                    packet.Write(player.id);
                    packet.Write(player.username);
                    packet.Write(room.playersReady[playerId]);
                }

                SendTCPData(toClient, packet);
            }
        }

        public static void PlayerJoinedRoom(int client, int roomId)
        {
            using (Packet packet = new Packet((int)ServerPackets.playerJoinedRoom))
            {
                packet.Write(client);
                packet.Write(Server.clients[client].username);

                SendTCPDataToAll(Server.rooms[roomId].playersReady.Keys, packet);
            }
        }

        public static void PlayerLeftRoom(int client, int roomId)
        {
            using (Packet packet = new Packet((int)ServerPackets.playerLeftRoom))
            {
                packet.Write(client);

                SendTCPDataToAll(Server.rooms[roomId].playersReady.Keys, packet);
            }
        }

        public static void PlayerReadyStateChange(int client)
        {
            using (Packet packet = new Packet((int)ServerPackets.playerReadyStateChange))
            {
                packet.Write(client);
                packet.Write(Server.clients[client].room.playersReady[client]);

                SendTCPDataToAll(Server.clients[client].room.playersReady.Keys, packet);
            }
        }

        public static void GameStarted(RoomManager room, Map map)
        {
            using (Packet packet = new Packet((int)ServerPackets.gameStarted))
            {
                Dictionary<Position, Tile> tiles = map.GetTiles();
                packet.Write(tiles.Count);
                foreach (Tile tile in tiles.Values)
                {
                    packet.Write(tile.Position.x);
                    packet.Write(tile.Position.y);
                    packet.Write((int)tile.Biome);
                }

                SendTCPDataToAll(room.playersReady.Keys, packet);
            }
        }

        public static void GameEnded(RoomManager room)
        {
            using (Packet packet = new Packet((int)ServerPackets.gameEnded))
            {
                SendTCPDataToAll(room.playersReady.Keys, packet);
            }
        }

        public static void VillageBuilt(RoomManager room, int clientId, int x, int y)
        {
            using (Packet packet = new Packet((int)ServerPackets.villageBuilt))
            {
                packet.Write(clientId);
                packet.Write(x);
                packet.Write(y);

                SendTCPDataToAll(room.playersReady.Keys, packet);
            }
        }

        public static void RoadBuilt(RoomManager room, int clientId, int x, int y)
        {
            using (Packet packet = new Packet((int)ServerPackets.roadBuilt))
            {
                packet.Write(clientId);
                packet.Write(x);
                packet.Write(y);

                SendTCPDataToAll(room.playersReady.Keys, packet);
            }
        }
        
        public static void CaravanBuilt(RoomManager room, int clientId, int x, int y, int caravanId)
        {
            using (Packet packet = new Packet((int)ServerPackets.caravanBuilt))
            {
                packet.Write(clientId);
                packet.Write(x);
                packet.Write(y);
                packet.Write(caravanId);

                SendTCPDataToAll(room.playersReady.Keys, packet);
            }
        }

        internal static void SetMovableTargetTile(RoomManager room, int fromClient, int movableId, int tileX, int tileY)
        {
            using (Packet packet = new Packet((int)ServerPackets.setMovableTargetTile))
            {
                packet.Write(fromClient);
                packet.Write(movableId);
                packet.Write(tileX);
                packet.Write(tileY);

                SendTCPDataToAll(room.playersReady.Keys, packet);
            }
        }

        public static void BuildingGatheringTileSet(RoomManager room, int playerId, int buildX, int buildY, int gatherTileX, int gatherTileY)
        {
            using (Packet packet = new Packet((int)ServerPackets.buildingGatheringTileSet))
            {
                packet.Write(playerId);
                packet.Write(buildX);
                packet.Write(buildY);
                packet.Write(gatherTileX);
                packet.Write(gatherTileY);

                SendTCPDataToAll(room.playersReady.Keys, packet);
            }
        }

        internal static void SpawnWarrior(RoomManager room, int playerId, int movableId, int warriorStrength)
        {
            using (Packet packet = new Packet((int)ServerPackets.spawnWarrior))
            {
                packet.Write(playerId);
                packet.Write(movableId);
                packet.Write(warriorStrength);

                SendTCPDataToAll(room.playersReady.Keys, packet);
            }
        }

        public static void RGBuildingResourcesChanged(ResourceGathering rgBuilding)
        {
            using (Packet packet = new Packet((int)ServerPackets.playerResourcesChanged))
            {
                packet.Write(rgBuilding.Tile.Position.x);
                packet.Write(rgBuilding.Tile.Position.y);

                packet.Write(rgBuilding.Resources.Count);
                foreach (KeyValuePair<Resource, int> resourceCount in rgBuilding.Resources)
                {
                    packet.Write((int)resourceCount.Key);
                    packet.Write(resourceCount.Value);
                }

                if (Server.TryGetRoomPlayers(rgBuilding.Owner.Id, out IEnumerable<int> clients))
                    SendTCPDataToAll(clients, packet);
            }
        }

        public static void MovableMoved(Player owner, int movableId, Tile tile)
        {
            using (Packet packet = new Packet((int)ServerPackets.movableMoved))
            {
                packet.Write(movableId);
                packet.Write(tile.Position.x);
                packet.Write(tile.Position.y);

                if (Server.TryGetRoomPlayers(owner.Id, out IEnumerable<int> clients))
                    SendTCPDataToAll(clients, packet);
            }
        }

        public static void MovableInventoryChanged(Movable movable)
        {
            using (Packet packet = new Packet((int)ServerPackets.movableInventoryChanged))
            {
                packet.Write(movable.ID);
                Dictionary<Resource, int> resources = movable.Resources;
                packet.Write(resources.Count);
                foreach (KeyValuePair<Resource, int> keyValuePair in resources)
                {
                    packet.Write((int)keyValuePair.Key);
                    packet.Write(keyValuePair.Value);
                }

                if (Server.TryGetRoomPlayers(movable.Owner.Id, out IEnumerable<int> clients))
                    SendTCPDataToAll(clients, packet);
            }
        }
        #endregion

    }
}
