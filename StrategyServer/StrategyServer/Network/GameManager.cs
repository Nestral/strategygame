using System.Collections.Generic;

namespace StrategyServer
{
    class GameManager
    {
        public static List<GameObject> gameObjects = new List<GameObject>();

        public static void Update()
        {
            foreach (GameObject gameObject in gameObjects)
                gameObject.Update(Constants.MS_PER_TICK / 1000f);

            ThreadManager.UpdateMain();
        }
    }
}
