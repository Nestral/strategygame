using System;
using System.Collections.Generic;

namespace StrategyServer
{
    class ServerHandle
    {
        #region Packets
        public static void WelcomeReceived(int fromClient, Packet packet)
        {
            int clientIdCheck = packet.ReadInt();
            string username = packet.ReadString();

            if (username.Trim().Length == 0)
            {
                //No name
                Console.WriteLine("Player can't connect, because he doesn't have a name.");
                Server.clients[fromClient].Disconnect();
                return;
            }

            Console.WriteLine($"{Server.clients[fromClient].tcp.socket.Client.RemoteEndPoint} connected successfully and is now player {fromClient}.");
            if (fromClient != clientIdCheck)
            {
                Console.WriteLine($"Player \"{username}\" (ID: {fromClient}) has assumed the wrong client ID ({clientIdCheck})!");
                Server.clients[fromClient].Disconnect();
                return;
            }

            Server.clients[fromClient].username = username;
        }

        public static void RoomsStateRequestReceived(int fromClient, Packet packet)
        {
            ServerSend.RoomsState(fromClient);
        }

        public static void NewRoomReceived(int fromClient, Packet packet)
        {
            RoomManager newRoom = Server.CreateNewRoom(fromClient);

            if (newRoom == null)
            {
                Console.WriteLine($"Player {fromClient} is trying to create a room, but no more can be created.");
                //TODO: send a message to client
            }
            else
            {
                Console.WriteLine($"Player {fromClient} has created a new room {newRoom.roomId}.");
                ServerSend.RoomHosted(fromClient, newRoom.roomId);
            }
        }

        public static void PlayerJoinedRoomReceived(int fromClient, Packet packet)
        {
            int clientId = packet.ReadInt();
            int roomId = packet.ReadInt();

            if (!Server.rooms.ContainsKey(roomId))
            {
                // Console.WriteLine($"Player {fromClient} trying to join room {roomId} which doesn't exist.");
                return;
            }

            if (!Server.rooms[roomId].PlayerJoin(fromClient))
            {
                Console.WriteLine($"Player {fromClient} couldn't join room {roomId}, because it is already full.");
                return;
            }

            Console.WriteLine($"Player {fromClient} has joined the room {roomId}.");

            ServerSend.RoomInfo(fromClient, roomId);
            ServerSend.PlayerJoinedRoom(fromClient, roomId);
        }

        public static void PlayerLeftRoomReceived(int fromClient, Packet packet)
        {
            int clientId = packet.ReadInt();

            //Check if player is connected to a room
            if (Server.TryGetRoom(fromClient, out RoomManager room))
                return;

            int roomId = room.roomId;

            //Disconnect from room
            Console.WriteLine($"Player {fromClient} has left the room {roomId}.");

            ServerSend.PlayerLeftRoom(fromClient, roomId);
            room.PlayerDisconnect(fromClient);
        }

        public static void PlayerReadyStateChangeReceived(int fromClient, Packet packet)
        {
            int clientId = packet.ReadInt();

            if (!Server.TryGetRoom(fromClient, out RoomManager room))
                return;

            room.PlayerChangeReadyState(fromClient);

            ServerSend.PlayerReadyStateChange(fromClient);
            Console.WriteLine($"Player {fromClient} has changed his ready state.");
        }

        public static void VillageBuildReceived(int fromClient, Packet packet)
        {
            int clientId = packet.ReadInt();
            int x = packet.ReadInt();
            int y = packet.ReadInt();

            if (Server.TryGetRoom(fromClient, out RoomManager room)
                && room.game.BuildVillage(fromClient, x, y))
            {
                ServerSend.VillageBuilt(room, fromClient, x, y);
                Console.WriteLine($"Player {fromClient} has built a village on tile (X: {x}, Y: {y}).");
            }
        }

        public static void RoadBuildReceived(int fromClient, Packet packet)
        {
            int clientId = packet.ReadInt();
            int roadX = packet.ReadInt();
            int roadY = packet.ReadInt();

            if (Server.TryGetRoom(fromClient, out RoomManager room)
                && room.game.BuildRoad(fromClient, roadX, roadY))
            {
                ServerSend.RoadBuilt(room, fromClient, roadX, roadY);
                Console.WriteLine($"Player {fromClient} has built a road on tile (X: {roadX}, Y: {roadY}).");
            }
        }

        public static void CaravanBuildReceived(int fromClient, Packet packet)
        {
            int clientId = packet.ReadInt();
            int x = packet.ReadInt();
            int y = packet.ReadInt();

            if (Server.TryGetRoom(fromClient, out RoomManager room)
                && room.game.BuildCaravan(fromClient, x, y, out int movableId))
            {
                ServerSend.CaravanBuilt(room, clientId, x, y, movableId);
                Console.WriteLine($"Player {fromClient} has built a caravan on tile (X: {x}, Y: {y}).");
            }
        }

        public static void SetBuildingGatheringTileReceived(int fromClient, Packet packet)
        {
            int clientId = packet.ReadInt();
            int buildX = packet.ReadInt();
            int buildY = packet.ReadInt();
            int gatherTileX = packet.ReadInt();
            int gatherTileY = packet.ReadInt();

            if (Server.TryGetRoom(fromClient, out RoomManager room)
                && room.game.SetBuildingGatheringTile(fromClient, buildX, buildY, gatherTileX, gatherTileY))
            {
                ServerSend.BuildingGatheringTileSet(room, fromClient, buildX, buildY, gatherTileX, gatherTileY);
                Console.WriteLine($"Building on tile (X : {buildX}, Y : {buildY}) now collects resources from tile (X : {gatherTileX}, Y : {gatherTileY}).");
            }
        }

        public static void SetMovableTargetTileReceived(int fromClient, Packet packet)
        {
            int clientId = packet.ReadInt();
            int movableId = packet.ReadInt();
            int tileX = packet.ReadInt();
            int tileY = packet.ReadInt();

            if (Server.TryGetRoom(fromClient, out RoomManager room)
                && room.game.SetMovableTargetTile(fromClient, movableId, tileX, tileY))
            {
                ServerSend.SetMovableTargetTile(room, fromClient, movableId, tileX, tileY);
                Console.WriteLine($"Movable (ID : {movableId}) is now going to tile (X : {tileX}, Y : {tileY}).");
            }
        }

        public static void SpawnWarriorReceived(int fromClient, Packet packet)
        {
            int clientId = packet.ReadInt();
            int movableId = packet.ReadInt();

            if (Server.TryGetRoom(fromClient, out RoomManager room)
                && room.game.SpawnWarrior(fromClient, movableId, 1))
            {
                ServerSend.SpawnWarrior(room, fromClient, movableId, 1);
                Console.WriteLine($"Player {fromClient} has spawned a warrior in movable (ID: {movableId}).");
            }
        }

        public static void LoadResourcesToMovableReceived(int fromClient, Packet packet)
        {
            int clientId = packet.ReadInt();
            int movableId = packet.ReadInt();

            Dictionary<Resource, int> resources = new Dictionary<Resource, int>();
            int resourcesCount = packet.ReadInt();
            for (int i = 0; i < resourcesCount; i++)
            {
                Resource resource = (Resource)packet.ReadInt();
                int count = packet.ReadInt();
                if (!resources.TryAdd(resource, count))
                    return;
            }

            if (Server.TryGetRoom(fromClient, out RoomManager room)
                && room.game.LoadResourcesToMovable(movableId, resources)
                && room.game.TryGetMovable(movableId, out Movable movable))     // Unnecessary check
            {
                ServerSend.MovableInventoryChanged(movable);
                Console.WriteLine($"Resource loaded to movable(ID:{movableId}).");
            }
        }

        public static void UnloadResourcesFromMovableReceived(int fromClient, Packet packet)
        {
            int clientId = packet.ReadInt();
            int movableId = packet.ReadInt();

            Dictionary<Resource, int> resources = new Dictionary<Resource, int>();
            int resourcesCount = packet.ReadInt();
            for (int i = 0; i < resourcesCount; i++)
            {
                Resource resource = (Resource)packet.ReadInt();
                int count = packet.ReadInt();
                if (!resources.TryAdd(resource, count))
                    return;
            }

            if (Server.TryGetRoom(fromClient, out RoomManager room)
                && room.game.UnloadResourcesFromMovable(movableId, resources)
                && room.game.TryGetMovable(movableId, out Movable movable))                 // Unnecessary check
            {
                ServerSend.MovableInventoryChanged(movable);
                Console.WriteLine($"Resource unloaded from movable(ID:{movableId}).");
            }
        }
        #endregion

        public static void PlayerDisconnect(int client)
        {
            RoomManager room = Server.clients[client].room;
            if (room != null)
                room.PlayerDisconnect(client);
        }
    }
}
