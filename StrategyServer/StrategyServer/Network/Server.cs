using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;

namespace StrategyServer
{
    class Server
    {
        public const int MAX_ROOMS_COUNT = 10;
        public const int ROOM_MAX_PLAYERS = 10;
        public const int ROOM_MIN_PLAYERS = 1;

        public static int MaxPlayers { get; private set; }
        public static int Port { get; private set; }

        public static Dictionary<int, Client> clients = new Dictionary<int, Client>();
        public static Dictionary<int, RoomManager> rooms = new Dictionary<int, RoomManager>();

        public delegate void PacketHandler(int fromClient, Packet packet);
        public static Dictionary<int, PacketHandler> packetHandlers;

        private static TcpListener tcpListener;

        public static void Start(int maxPlayers, int port)
        {
            MaxPlayers = maxPlayers;
            Port = port;

            Console.WriteLine("Server starting...");
            InitializeServerData();

            tcpListener = new TcpListener(IPAddress.Any, Port);
            tcpListener.Start();
            tcpListener.BeginAcceptTcpClient(new AsyncCallback(TCPConnectCallback), null);

            string serverIP = new WebClient().DownloadString("http://icanhazip.com").TrimEnd(); //Dns.GetHostEntry(Dns.GetHostName()).AddressList[0].ToString();
            Console.WriteLine($"Server started. IP: {serverIP}. Port: {Port}.");
        }

        public static RoomManager CreateNewRoom(int fromClient)
        {
            //Create new room
            int newRoomId = GetEmptyRoomId();

            if (newRoomId < 0)
                return null;

            RoomManager newRoom = new RoomManager(ROOM_MIN_PLAYERS, ROOM_MAX_PLAYERS, newRoomId);
            rooms.Add(newRoomId, newRoom);

            //Connect player to room
            newRoom.PlayerJoin(fromClient);
            clients[fromClient].room = newRoom;

            return newRoom;
        }

        public static void RemoveRoom(RoomManager room)
        {
            Console.WriteLine($"Removing room {room.roomId}");

            //Disconnect players if some are left in the room
            foreach (int client in room.playersReady.Keys)
            {
                clients[client].room = null;
            }

            //Remove room
            rooms.Remove(room.roomId);
            room.game.ResetAll();
            room = null;
        }

        public static bool TryGetRoom(int clientId, out RoomManager room)
        {
            room = null;
            if (!clients.TryGetValue(clientId, out Client client)
                || client.room == null)
                return false;
            
            room = client.room;
            return true;
        }

        public static bool TryGetRoomPlayers(int clientId, out IEnumerable<int> clientIds)
        {
            clientIds = null;
            if (!TryGetRoom(clientId, out RoomManager room))
                return false;

            clientIds = room.playersReady.Keys;
            return true;
        }

        private static void TCPConnectCallback(IAsyncResult result)
        {
            TcpClient client = tcpListener.EndAcceptTcpClient(result);
            tcpListener.BeginAcceptTcpClient(new AsyncCallback(TCPConnectCallback), null);

            Console.WriteLine($"Incoming connection from {client.Client.RemoteEndPoint}...");

            for (int i = 1; i <= MaxPlayers; i++)
            {
                if (clients[i].tcp.socket == null)
                {
                    clients[i].tcp.Connect(client);
                    return;
                }
            }

            Console.WriteLine($"{client.Client.RemoteEndPoint} failed to connect: Server is full!");
        }

        private static void InitializeServerData()
        {
            for (int i = 1; i <= MaxPlayers; i++)
            {
                clients.Add(i, new Client(i));
            }

            packetHandlers = new Dictionary<int, PacketHandler>()
            {
                { (int)ClientPackets.welcomeReceived, ServerHandle.WelcomeReceived },
                { (int)ClientPackets.roomsStateRequest, ServerHandle.RoomsStateRequestReceived },
                { (int)ClientPackets.createNewRoomRequest, ServerHandle.NewRoomReceived },
                { (int)ClientPackets.playerJoinRoomRequest, ServerHandle.PlayerJoinedRoomReceived },
                { (int)ClientPackets.playerLeaveRoomRequest, ServerHandle.PlayerLeftRoomReceived },
                { (int)ClientPackets.playerReadyStateChangeRequest, ServerHandle.PlayerReadyStateChangeReceived },
                { (int)ClientPackets.buildVillageRequest, ServerHandle.VillageBuildReceived },
                { (int)ClientPackets.buildRoadRequest, ServerHandle.RoadBuildReceived },
                { (int)ClientPackets.setBuildingGatheringTileRequest, ServerHandle.SetBuildingGatheringTileReceived },
                { (int)ClientPackets.buildCaravanRequest, ServerHandle.CaravanBuildReceived },
                { (int)ClientPackets.setMovableTargetTileRequest, ServerHandle.SetMovableTargetTileReceived },
                { (int)ClientPackets.spawnWarriorRequest, ServerHandle.SpawnWarriorReceived },
                { (int)ClientPackets.loadResourcesToMovableRequest, ServerHandle.LoadResourcesToMovableReceived },
                { (int)ClientPackets.unloadResourcesFromMovableRequest, ServerHandle.UnloadResourcesFromMovableReceived }
            };
            Console.WriteLine("Initialized packets.");
        }

        private static int GetEmptyRoomId()
        {
            for (int i = 1; i <= MAX_ROOMS_COUNT; i++)
            {
                if (!rooms.ContainsKey(i))
                    return i;
            }

            return -1;
        }
    }
}
