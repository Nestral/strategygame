﻿using System.Collections.Generic;
using System.Linq;

namespace StrategyServer
{
    abstract class Building : GameObject
    {
        public Tile Tile { get; protected set; }
        public Player Owner { get; protected set; }
        public bool AllowMovablesThrough { get; protected set; }

        public List<Tile> connectedTiles;
        public List<Building> ConnectedBuildings => connectedTiles.Where(tile => tile.Building != null).Select(tile => tile.Building).ToList();

        public RoadSystem RoadSystem { get; set; }

        public abstract Dictionary<Resource, int> BuildingPrice { get; }

        public Building(Tile tile, Player owner) : base(tile.Map.game)
        {
            Tile = tile;
            Owner = owner;

            connectedTiles = new List<Tile>();

            // Connect near roads to the building
            foreach (Tile connectedTile in Tile.GetNeighbourTiles())
                if (connectedTile.Building != null && connectedTile.Building.AllowMovablesThrough)
                    connectedTile.Building.connectedTiles.Add(Tile);
        }

        public Map Map => Tile.Map;
        public Game Game => Tile.Map.game;
        public GameSettings GameSettings => Tile.Map.game.gameSettings;

        public void ConnectToRoadSystem()
        {
            // Connect to a nearby RoadSystem if possible
            bool connected = false;
            foreach (Tile connectedTile in Tile.GetNeighbourTiles())
            {
                if (connectedTile.Building != null && connectedTile.Building.AllowMovablesThrough)
                {
                    // Connect to a road
                    connectedTiles.Add(connectedTile);

                    // If we have not connected to a road system yet, then connect the road to the road system
                    if (!connected)
                    {
                        connectedTile.Building.RoadSystem.AddBuilding(this);
                        connected = true;
                    }
                    // Otherwise connect two road systems
                    else if (connectedTile.Building.RoadSystem != RoadSystem)
                        connectedTile.Building.RoadSystem.AddRoadSystem(this);
                }
                else if (connectedTile.Building != null)
                {
                    // Connect to a building
                    connectedTiles.Add(connectedTile);
                }
            }

            if (!connected)
                RoadSystem = new RoadSystem(this);
        }

        public bool ConnectTile(Tile connectTile)
        {
            // Check if connectTile is accesible
            if (Tile.GetNeighbourTiles().Contains(connectTile))
            {
                // Connect tile
                connectedTiles.Add(connectTile);
                return true;
            }

            return false;
        }
    }
}