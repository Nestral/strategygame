using System;
using System.Collections.Generic;

namespace StrategyServer
{
    public class Inventory<T>
    {
        public int SlotsCount { get; }
        public int StackSize { get; }

        public int StacksCount { get; private set; }

        public Dictionary<T, int> collection;

        public Inventory(int slotsCount, int stackSize)
        {
            SlotsCount = slotsCount;
            StackSize = stackSize;

            StacksCount = 0;
            collection = new Dictionary<T, int>();
        }

        public bool Contains(T item) => collection.ContainsKey(item);
        public bool TryGetItemCount(T item, out int value) => collection.TryGetValue(item, out value);

        public bool Contains(Dictionary<T, int> items)
        {
            foreach (KeyValuePair<T, int> keyValuePair in items)
            {
                if (!TryGetItemCount(keyValuePair.Key, out int value)
                    || value < keyValuePair.Value)
                    return false;
            }

            return true;
        }

        public bool UnloadItem(T item, int count)
        {
            if (collection.TryGetValue(item, out int itemCount) && count <= itemCount)
            {
                int remainder = collection[item] % StackSize;
                while (count > 0)
                {
                    if (count <= remainder)
                    {
                        collection[item] -= count;
                        return true;
                    }
                    else if (remainder > 0)
                    {
                        collection[item] -= remainder;
                        remainder = 0;
                        count -= remainder;
                    }
                    else
                    {
                        int subCount = Math.Min(count, StackSize);
                        collection[item] -= subCount;
                        count -= subCount;
                        remainder = StackSize - subCount;
                        StacksCount--;
                    }
                }
                return true;
            }

            return false;
        }

        public void LoadItem(T item, int count, out int extra)
        {
            extra = count;
            if (Contains(item))
            {
                int remainder = StackSize - collection[item] % StackSize;
                while (extra > 0 && (StacksCount < SlotsCount || remainder > 0))
                {
                    if (extra <= remainder)
                    {
                        collection[item] += extra;
                        extra = 0;
                        return;
                    }
                    else if (remainder > 0)
                    {
                        collection[item] += remainder;
                        remainder = 0;
                        extra -= remainder;
                    }
                    else
                    {
                        int addCount = Math.Min(extra, StackSize);
                        collection[item] += addCount;
                        extra -= addCount;
                        remainder = StackSize - addCount;
                        StacksCount++;
                    }
                }
            }
            else if (StacksCount < SlotsCount)
            {
                collection.Add(item, 0);
                while (extra > 0)
                {
                    int subCount = Math.Min(extra, StackSize);
                    extra -= subCount;
                    collection[item] += subCount;
                    StacksCount++;
                }
            }
        }

    }
}