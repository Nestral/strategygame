﻿using System;
using System.Collections.Generic;

namespace StrategyServer
{
    abstract class ResourceGathering : Building
    {
        public abstract float ResourceGatheringDelay { get; }
        public abstract int ResourceGatheringAmount { get; }
        public abstract int ResourceGatheringDistance { get; }
        public Tile ResourceGatheringTile { get; protected set; }

        public float TimeUntilGathering { get; private set; }

        public Inventory<Resource> inventory;

        public ResourceGathering(Tile tile, Player owner) : base(tile, owner) { }

        public bool HasInventory => inventory?.SlotsCount > 0;
        public int InventorySlotsCount => inventory == null ? 0 : inventory.SlotsCount;
        public int InventoryStackSize => inventory == null ? 0 : inventory.StackSize;
        public int InventoryStacksCount => inventory == null ? 0 : inventory.StacksCount;

        public Dictionary<Resource, int> Resources => inventory?.collection;

        public void LoadResource(Resource resource, int count, out int extra) => inventory.LoadItem(resource, count, out extra);
        public bool UnloadResource(Resource resource, int count) => inventory.UnloadItem(resource, count);
        public bool HasResources(Dictionary<Resource, int> resources) => inventory.Contains(resources);

        public bool SetGatheringTile(Tile tile)
        {
            if (Tile.Distance(tile) <= ResourceGatheringDistance)
            {
                ResourceGatheringTile = tile;
                return true;
            }

            return false;
        }

        public override void Update(float deltaTime)
        {
            if (ResourceGatheringTile == null)
            {
                TimeUntilGathering = ResourceGatheringDelay;
                return;
            }

            TimeUntilGathering -= deltaTime;
            if (TimeUntilGathering <= 0)
                GatherResources();
        }

        private void GatherResources()
        {
            if (ResourceGatheringTile.Resource == Resource.None)
                return;
                
            LoadResource(ResourceGatheringTile.Resource, ResourceGatheringAmount, out int _);
            TimeUntilGathering += ResourceGatheringDelay;

            ServerSend.RGBuildingResourcesChanged(this);
        }
    }
}