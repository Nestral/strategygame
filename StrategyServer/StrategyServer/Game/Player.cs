using System;
using System.Collections.Generic;
using System.Linq;

namespace StrategyServer
{
    class Player
    {
        public int Id { get; }

        public Player(int id)
        {
            Id = id;
        }
    }
}