using System.Collections.Generic;
using System.Linq;

namespace StrategyServer
{
    class Road : Building
    {
        public Road(Tile tile, Player owner) : base(tile, owner)
        {
            AllowMovablesThrough = true;
        }

        private Dictionary<Resource, int> price = new Dictionary<Resource, int>()
        {
            {Resource.Wood, 1},
            {Resource.Clay, 1},
            {Resource.Stone, 0},
            {Resource.Wheat, 0}
        };
        public override Dictionary<Resource, int> BuildingPrice => price;

        public override string ToString() => "Road";
    }
}