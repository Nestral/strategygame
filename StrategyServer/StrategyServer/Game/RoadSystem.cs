using System.Collections.Generic;

namespace StrategyServer
{
    class RoadSystem
    {
        public List<Building> buildings;

        public delegate void RoadSystemUpdateHandler();
        public event RoadSystemUpdateHandler RoadSystemUpdated;

        public delegate void RoadSystemRemoveHandler(RoadSystem newRoadSystem);
        public event RoadSystemRemoveHandler RoadSystemRemoved;

        public RoadSystem(Building startBuilding)
        {
            buildings = new List<Building>();
            buildings.Add(startBuilding);
        }

        public bool AddBuilding(Building building)
        {
            // Check if building is accesible by this RoadSystem
            if (!building.AllowMovablesThrough || !IsTileAccesible(building.Tile))
                return false;

            // Add building to RoadSystem
            buildings.Add(building);
            building.RoadSystem = this;

            // Invoke an update event
            RoadSystemUpdated?.Invoke();

            return true;
        }

        public bool AddRoadSystem(Building building)
        {
            // Check if building is accesible by this RoadSystem
            if (!building.AllowMovablesThrough || !IsTileAccesible(building.Tile))
                return false;

            // Remember the old road system
            RoadSystem oldRS = building.RoadSystem;

            // Add roads connected to this road
            Building[] connectedBuldings = building.RoadSystem.buildings.ToArray();

            foreach (Building connectedBuilding in connectedBuldings)
            {
                buildings.Add(connectedBuilding);
            }

            // Set roads' RoadSystem to this
            foreach (Building connectedBuilding in connectedBuldings)
            {
                connectedBuilding.RoadSystem = this;
            }

            // Invoke a RoadSystemRemoved event
            oldRS.RoadSystemRemoved?.Invoke(this);
            // Invoke an RoadSystemUpdated event
            RoadSystemUpdated?.Invoke();


            return true;
        }

        public bool IsTileAccesible(Tile tile)
        {
            foreach (Building building in buildings)
            {
                if (building.Tile == tile || building.connectedTiles.Contains(tile))
                    return true;
            }

            return false;
        }

        public Tile[] FindPath(Tile fromTile, Tile toTile)
        {
            if (!(fromTile.Building is Building building) || !IsTileAccesible(toTile))
                return null;

            List<Building> nextBuildings = new List<Building>() { building };
            Dictionary<Position, int> buildingsDist = new Dictionary<Position, int>() { { building.Tile.Position, 0 } };

            return Pathfind(toTile, nextBuildings, buildingsDist, 0);
        }

        private Tile[] Pathfind(Tile targetTile, List<Building> nextBuildings, Dictionary<Position, int> buildingsDist, int currentDistance)
        {
            List<Building> _nextBuildings = new List<Building>();
            foreach (Building build in nextBuildings)
            {
                foreach (Tile connectedTile in build.connectedTiles)
                {
                    Building connectedBuild = connectedTile.Building;
                    if (!connectedBuild.AllowMovablesThrough || buildingsDist.ContainsKey(connectedTile.Position))
                        continue;

                    if (connectedTile == targetTile)
                    {
                        Tile[] path = new Tile[currentDistance + 2];
                        path[currentDistance + 1] = connectedTile;
                        return Pathback(path, buildingsDist, connectedBuild, currentDistance);
                    }

                    buildingsDist.Add(connectedTile.Position, currentDistance + 1);
                    _nextBuildings.Add(connectedBuild);
                }
            }

            return _nextBuildings.Count == 0
                ? null
                : Pathfind(targetTile, _nextBuildings, buildingsDist, currentDistance + 1);
        }

        private Tile[] Pathback(Tile[] path, Dictionary<Position, int> buildingsDist, Building building, int distance)
        {
            foreach (Building connectedBuild in building.ConnectedBuildings)
            {
                if (buildingsDist.TryGetValue(connectedBuild.Tile.Position, out int dist) && dist == distance)
                {
                    path[distance] = connectedBuild.Tile;
                    break;
                }
            }

            if (distance == 0)
                return path;

            return Pathback(path, buildingsDist, path[distance].Building, distance - 1);
        }
    }
}