using System.Collections.Generic;

namespace StrategyServer
{
    class Map
    {
        internal Game game;

        private Dictionary<Position, Tile> tiles = new Dictionary<Position, Tile>();

        public Map(Game game) 
        { 
            this.game = game;
        }

        public void Reset()
        {
            GameManager.gameObjects.Clear();
            tiles.Clear();
        }

        public Dictionary<Position, Tile> GetTiles() => tiles;

        public void SpawnTile(Tile tile, Position position)
        {
            tiles.Add(position, tile);
        }

        public bool TryGetTile(Position position, out Tile tile)
        {
            return tiles.TryGetValue(position, out tile);
        }

    }
}