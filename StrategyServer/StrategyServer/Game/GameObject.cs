﻿namespace StrategyServer
{
    abstract class GameObject
    {
        private Game game;

        public virtual void Update(float deltaTime) { }

        public GameObject(Game game)
        {
            this.game = game;

            GameManager.gameObjects.Add(this);
            game.gameObjects.Add(this);
        }

        public void Destroy()
        {
            GameManager.gameObjects.Remove(this);
            game.gameObjects.Remove(this);
        }
    }
}