﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace StrategyServer
{
    abstract class Movable : GameObject
    {
        public Player Owner { get; protected set; }
        public int ID { get; protected set; }

        public List<Warrior> warriors;
        public int WarriorsPower { get; private set; }

        // Movement speed in Tiles/second
        public abstract float MoveSpeed { get; }

        public Tile CurrentTile { get; protected set; }
        protected float currentTileMovingProgress;
        public Tile DestinationTile { get; private set; }
        protected Tile[] path;
        protected int currentTileIndex;

        public Inventory<Resource> inventory;

        public abstract Dictionary<Resource, int> BuildingPrice { get; }

        public Movable(Tile tile, Player owner, int id) : base(tile.Map.game)
        {
            CurrentTile = tile;
            Owner = owner;
            ID = id;
            currentTileMovingProgress = 0.5f;

            warriors = new List<Warrior>();
            WarriorsPower = 0;
        }

        public bool HasInventory => inventory?.SlotsCount > 0;
        public int InventorySlotsCount => inventory == null ? 0 : inventory.SlotsCount;
        public int InventoryStackSize => inventory == null ? 0 : inventory.StackSize;
        public int InventoryStacksCount => inventory == null ? 0 : inventory.StacksCount;

        public Dictionary<Resource, int> Resources => inventory?.collection;

        public void LoadResource(Resource resource, int count, out int extra) => inventory.LoadItem(resource, count, out extra);
        public bool UnloadResource(Resource resource, int count) => inventory.UnloadItem(resource, count);
        public bool HasResources(Dictionary<Resource, int> resources) => inventory.Contains(resources);

        public bool TryLoadResourcesFromBuilding(Dictionary<Resource, int> resources)
        {
            // Check if movable is in the building, that has enough resources
            if (CurrentTile.Building == null || !(CurrentTile.Building is ResourceGathering building)
                || !building.HasResources(resources))
                return false;

            // Load resources
            foreach (KeyValuePair<Resource, int> keyValuePair in resources)
            {
                LoadResource(keyValuePair.Key, keyValuePair.Value, out int extra);
                building.UnloadResource(keyValuePair.Key, keyValuePair.Value - extra);
            }
            return true;        
        }

        public bool TryUnloadResourcesToBuilding(Dictionary<Resource, int> resources)
        {
            // Check if movable is in the building, and movable has enough resources
            if (CurrentTile.Building == null || !(CurrentTile.Building is ResourceGathering building)
                || !HasResources(resources))
                return false;

            // Unload resources
            foreach (KeyValuePair<Resource, int> keyValuePair in resources)
            {
                building.LoadResource(keyValuePair.Key, keyValuePair.Value, out int extra);
                UnloadResource(keyValuePair.Key, keyValuePair.Value - extra);
            }
            return true;    
        }

        public bool SetDestinationTile(Tile tile)
        {
            DestinationTile = tile;

            // Remember previous and next tiles on the path for later
            Tile prevTile = (path == null || currentTileIndex == 0) ? null : path[currentTileIndex - 1];
            Tile nextTile = (path == null || path.Length <= currentTileIndex + 1) ? null : path[currentTileIndex + 1];

            // Calculate path if tile is not null, otherwise set it to null
            path = tile == null ? null : FindPath(DestinationTile);

            // Change currentTileMovingProgress value, if needed
            if (path != null && path.Length >= 2
                && (currentTileMovingProgress < 0.5f && prevTile != null && prevTile == path[1]
                || currentTileMovingProgress > 0.5f && nextTile != null && nextTile != path[1]))
            {
                currentTileMovingProgress = 1 - currentTileMovingProgress;
            }

            return true;
        }

        public override void Update(float deltaTime)
        {
            if (DestinationTile != null)
                MoveTowardsTile(DestinationTile, deltaTime);
        }

        protected abstract bool IsTileAccesible(Tile tile);
        protected abstract Tile[] FindPath(Tile toTile);

        protected void MoveTowardsTile(Tile tile, float deltaTime)
        {
            if (path == null || tile == CurrentTile && currentTileMovingProgress >= 0.5f)
                return;

            currentTileMovingProgress += MoveSpeed * deltaTime;
            if (currentTileMovingProgress >= 1)
            {
                currentTileMovingProgress--;
                MoveTileForward();
            }
        }

        protected void MoveTileForward()
        {
            if (path.Length <= currentTileIndex + 1)
                return;

            currentTileIndex++;
            CurrentTile = path[currentTileIndex];
            currentTileMovingProgress = 0f;

            ServerSend.MovableMoved(Owner, ID, CurrentTile);
        }

        internal bool SpawnWarrior(int warriorStrength)
        {
            Warrior warrior = new Warrior(warriorStrength);
            warriors.Add(warrior);
            WarriorsPower += warriorStrength;
            return true;
        }
    }
}