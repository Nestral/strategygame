namespace StrategyServer
{
    class Warrior
    {
        public int Strength { get; }

        public Warrior(int strength)
        {
            Strength = strength;
        }
    }
}