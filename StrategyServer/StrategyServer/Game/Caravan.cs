using System.Collections.Generic;

namespace StrategyServer
{
    class Caravan : Movable
    {
        public override float MoveSpeed => 0.5f;

        private Building CurrentBuilding => CurrentTile.Building;
        private RoadSystem roadSystem;

        public Caravan(Tile tile, Player owner, int id) : base(tile, owner, id)
        {
            // Add a reference to a road system
            roadSystem = CurrentBuilding.RoadSystem;

            inventory = new Inventory<Resource>(tile.Map.game.gameSettings.CaravansInventorySlotsCount, tile.Map.game.gameSettings.CaravansInventoryStackSize);

            // Subscribe to road system updates
            roadSystem.RoadSystemUpdated += OnRoadSystemUpdated;
            roadSystem.RoadSystemRemoved += OnRoadSystemRemoved;
        }

        protected override bool IsTileAccesible(Tile tile)
        {
            return CurrentBuilding.RoadSystem.IsTileAccesible(tile);
        }

        protected override Tile[] FindPath(Tile tile)
        {
            currentTileIndex = 0;
            return CurrentBuilding.RoadSystem.FindPath(CurrentTile, tile);
        }

        private void OnRoadSystemUpdated()
        {
            // Update path to destinationTile
            SetDestinationTile(DestinationTile);
        }

        private void OnRoadSystemRemoved(RoadSystem newRoadSystem)
        {
            // Unsubscribe from old road system's updates
            roadSystem.RoadSystemUpdated -= OnRoadSystemUpdated;
            roadSystem.RoadSystemRemoved -= OnRoadSystemRemoved;

            // Change road system reference
            roadSystem = newRoadSystem;

            // Subscribe to new road system's updates
            roadSystem.RoadSystemUpdated += OnRoadSystemUpdated;
            roadSystem.RoadSystemRemoved += OnRoadSystemRemoved;

            // No need to update path to destinationTile, because the RoadSystemUpdated event will be invoked
        }

        private Dictionary<Resource, int> price = new Dictionary<Resource, int>()
        {
            {Resource.Wood, 3},
            {Resource.Clay, 0},
            {Resource.Stone, 0},
            {Resource.Wheat, 1}
        };

        public override Dictionary<Resource, int> BuildingPrice => price;

        public override string ToString() => "Caravan";
    }
}