using System;
using System.Collections.Generic;

namespace StrategyServer
{
    class Tile
    {
        public Map Map { get; }
        public Position Position { get; }
        public Biome Biome { get; }
        public Resource Resource { get; }
        public Building Building { get; private set; }

        public Tile(Map map, int x, int y, Biome biome)
        {
            Map = map;
            Position = new Position(x, y);
            Biome = biome;
            Resource = BiomesResource[biome];

            Building = null;
        }

        public Tile(Map map, Position position, Biome biome)
        {
            Map = map;
            Position = position;
            Biome = biome;
            Resource = BiomesResource[biome];

            Building = null;
        }

        public bool Build(Building building, Player player)
        {
            if (Building == null && TryPayResources(building.BuildingPrice, player))
            {
                Building = building;
                Building.ConnectToRoadSystem();
                return true;
            }

            return false;
        }

        internal bool BuildMovable(Movable movable, Player player)
        {
            if (TryPayResources(movable.BuildingPrice, player))
            {
                
                return true;
            }

            return false;
        }

        public bool CanBuild(Building building, Player player)
        {
            return Building == null && CanPayResources(building.BuildingPrice, player);
        }

        public bool CanBuild(Movable movable, Player player)
        {
            return CanPayResources(movable.BuildingPrice, player);
        }

        private bool TryPayResources(Dictionary<Resource, int> buildingPrice, Player player)
        {
            return true;
        }

        private bool CanPayResources(Dictionary<Resource, int> buildingPrice, Player player)
        {
            return true;
        }

        public IEnumerable<Tile> GetNeighbourTiles()
        {
            Position[] neigbourPositions = GetNeighbourTilesPositions();
            foreach (Position position in neigbourPositions)
            {
                if (Map.TryGetTile(position, out Tile tile))
                    yield return tile;
            }
        }

        public Position[] GetNeighbourTilesPositions() => new Position[]
        {
            new Position(Position.x, Position.y + 1),
            new Position(Position.x + 1, Position.y),
            new Position(Position.x + 1, Position.y - 1),
            new Position(Position.x, Position.y - 1),
            new Position(Position.x - 1, Position.y),
            new Position(Position.x - 1, Position.y + 1)
        };

        public int Distance(Tile targetTile)
        {
            if (targetTile == this)
                return 0;

            Dictionary<Position, int> tilesDist = new Dictionary<Position, int>();
            List<Tile> nextTiles = new List<Tile>();

            nextTiles.Add(this);
            tilesDist.Add(Position, 0);

            return CalculateDistance(targetTile, 0, nextTiles, tilesDist);
        }

        private int CalculateDistance(Tile targetTile, int currentDistance, List<Tile> nextTiles, Dictionary<Position, int> tilesDist)
        {
            List<Tile> _nextTiles = new List<Tile>();
            foreach (Tile tile in nextTiles)
            {
                foreach (Tile neighbour in tile.GetNeighbourTiles())
                {
                    if (tilesDist.ContainsKey(neighbour.Position))
                        continue;

                    if (neighbour == targetTile)
                        return currentDistance + 1;

                    tilesDist.Add(neighbour.Position, currentDistance + 1);
                    _nextTiles.Add(neighbour);
                }
            }

            return _nextTiles.Count == 0
                ? int.MaxValue
                : CalculateDistance(targetTile, currentDistance + 1, _nextTiles, tilesDist);
        }

        public override bool Equals(object obj)
        {
            return obj is Tile tile &&
                   EqualityComparer<Map>.Default.Equals(Map, tile.Map) &&
                   EqualityComparer<Position>.Default.Equals(Position, tile.Position);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Map, Position);
        }

        public static Dictionary<Biome, Resource> BiomesResource = new Dictionary<Biome, Resource>()
            {
                { Biome.Forest, Resource.Wood },
                { Biome.Plains, Resource.Wheat },
                { Biome.Mountains, Resource.Stone },
                { Biome.Desert, Resource.None },
                { Biome.River, Resource.Clay },
                { Biome.Lake, Resource.Clay },
                { Biome.Ocean, Resource.None }
            };
    }

    struct Position
    {
        public int x;
        public int y;

        public Position(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override bool Equals(object obj)
        {
            return obj is Position position &&
                   x == position.x &&
                   y == position.y;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(x, y);
        }
    }

    enum Biome
    {
        Forest,
        Plains,
        Mountains,
        Desert,
        River,
        Lake,
        Ocean
    }

    enum Resource
    {
        Wood,
        Clay,
        Stone,
        Wheat,
        None
    }
}