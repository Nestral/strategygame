using System;
using System.Collections.Generic;
using System.Linq;

namespace StrategyServer
{
    class Game
    {
        public static Random random = new Random();

        public Dictionary<Resource, int> startingResources = new Dictionary<Resource, int>()
        {
            {Resource.Wood, 99},
            {Resource.Clay, 99},
            {Resource.Stone, 99},
            {Resource.Wheat, 99}
        };

        public bool IsStarted { get; private set; } = false;
        public bool IsFinished { get; private set; } = false;

        private int movableIdCounter;
        private Dictionary<int, Movable> movables;

        private Dictionary<int, Player> players;

        public List<GameObject> gameObjects;

        private readonly RoomManager room;
        internal Map map;
        internal GameSettings gameSettings;

        public Game(RoomManager _room)
        {
            players = new Dictionary<int, Player>();
            movables = new Dictionary<int, Movable>();
            gameObjects = new List<GameObject>();
            gameSettings = new GameSettings();

            room = _room;
        }

        public void ResetMap()
        {
            if (map == null)
                map = new Map(this);
            else
                map.Reset();

            IsStarted = false;
            IsFinished = false;
        }

        public void ResetAll()
        {
            foreach (GameObject gameObject in gameObjects)
            {
                gameObject.Destroy();
            }
        }

        public void StartGame(int[] clients, int minMapSize, int maxMapSize)
        {
            movables.Clear();
            movableIdCounter = 0;

            // Reset players
            players.Clear();
            foreach (int client in clients)
            {
                Player player = new Player(client);
                players.Add(client, player);
            }

            ResetMap();
            GenerateMap(minMapSize, maxMapSize);
            IsStarted = true;
        }

        public bool TryGetMovable(int movableId, out Movable movable) => movables.TryGetValue(movableId, out movable);

        public bool BuildVillage(int playerId, int x, int y)
        {
            Player player = players[playerId];

            // Return if no such tile exists or unable to build on that tile
            if (!map.TryGetTile(new Position(x, y), out Tile tile) || !tile.Build(new Village(tile, player), player))
                return false;

            return true;
        }

        public bool BuildRoad(int playerId, int roadX, int roadY)
        {
            Player player = players[playerId];

            // Return if no such tiles exist
            if (!map.TryGetTile(new Position(roadX, roadY), out Tile roadTile))
                return false;

            // Check if the road can be built here. (If there are buildings connected)
            bool connectable = false;
            foreach (Tile tile in roadTile.GetNeighbourTiles())
                if (tile.Building != null)
                {
                    connectable = true;
                    break;
                }
            
            if (!connectable)
                return false;

            // Return if unable to build on that tile
            Road newRoad = new Road(roadTile, player);
            if (!roadTile.Build(newRoad, player))
                return false;

            return true;
        }

        internal bool BuildCaravan(int playerId, int tileX, int tileY, out int movableId)
        {
            Player player = players[playerId];
            movableId = movableIdCounter;

            // Return false if no such tile exists or unable to build on that tile
            if (!map.TryGetTile(new Position(tileX, tileY), out Tile tile) || !(tile.Building is Road))
                return false;

            Caravan caravan = new Caravan(tile, player, movableIdCounter);
            if (!tile.BuildMovable(caravan, player))
                return false;
            movables.Add(movableIdCounter, caravan);
            movableIdCounter++;
            return true;
        }

        private void GenerateMap(int minMapSize, int maxMapSize)
        {
            Tile startTile = new Tile(map, 0, 0, Biome.Plains);
            map.SpawnTile(startTile, startTile.Position);

            List<Tile> nextTiles = new List<Tile>() { startTile };
            Dictionary<Position, int> tilesDist = new Dictionary<Position, int>() { { startTile.Position, 0 } };
            GenerateMap(minMapSize, maxMapSize, 1, nextTiles, tilesDist);
        }

        private void GenerateMap(int minMapSize, int maxMapSize, int currentDistance, List<Tile> nextTiles, Dictionary<Position, int> tilesDist)
        {
            if (nextTiles.Count == 0)
                return;

            List<Tile> _nextTiles = new List<Tile>();

            foreach (Tile tile in nextTiles)
            {
                if (currentDistance >= random.Next(minMapSize, maxMapSize + 1))
                    continue;

                foreach (Position neighbourPos in tile.GetNeighbourTilesPositions())
                {
                    if (!tilesDist.TryAdd(neighbourPos, currentDistance))
                        continue;

                    Tile newTile = SpawnRandomTile(neighbourPos);
                    _nextTiles.Add(newTile);
                }
            }

            GenerateMap(minMapSize, maxMapSize, currentDistance + 1, _nextTiles, tilesDist);
        }

        private Tile SpawnRandomTile(Position position)
        {
            int biomes = Enum.GetValues(typeof(Biome)).Cast<int>().Max();
            Biome biome = (Biome)(random.Next(biomes + 1));
            Tile tile = new Tile(map, position, biome);
            map.SpawnTile(tile, position);
            return tile;
        }

        private bool CheckGameFinish()
        {
            return false;
        }

        internal bool SetBuildingGatheringTile(int fromClient, int buildX, int buildY, int gatherTileX, int gatherTileY)
        {
            return (map.TryGetTile(new Position(buildX, buildY), out Tile buildTile)                // Check if tile exists
                && buildTile.Building != null && buildTile.Building.Owner == players[fromClient]    // Check if that tile has a building on it and it's owner is a given player
                && buildTile.Building is ResourceGathering resourceGathering                        // Check if that building can gather resources
                && map.TryGetTile(new Position(gatherTileX, gatherTileY), out Tile gatherTile)      // Check if tile exists
                && resourceGathering.SetGatheringTile(gatherTile));                                 // Check if it is possible to collect resources from that tile 
        }

        internal bool SetMovableTargetTile(int fromClient, int movableId, int tileX, int tileY)
        {
            return (map.TryGetTile(new Position(tileX, tileY), out Tile tile)
                // && tile.Building is Road road
                && movables.TryGetValue(movableId, out Movable movable)
                && movable.SetDestinationTile(tile));
        }

        internal bool SpawnWarrior(int fromClient, int movableId, int warriorStrength)
        {
            return (movables.TryGetValue(movableId, out Movable movable)
                && movable.SpawnWarrior(warriorStrength));
        }
        
        internal bool LoadResourcesToMovable(int movableId, Dictionary<Resource, int> resources)
        {
            return (movables.TryGetValue(movableId, out Movable movable)
                && movable.TryLoadResourcesFromBuilding(resources));
        }

        internal bool UnloadResourcesFromMovable(int movableId, Dictionary<Resource, int> resources)
        {
            return (movables.TryGetValue(movableId, out Movable movable)
                && movable.TryUnloadResourcesToBuilding(resources));
        }
    }
}