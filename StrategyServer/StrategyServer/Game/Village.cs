using System.Collections.Generic;

namespace StrategyServer
{
    class Village : ResourceGathering
    {
        public override float ResourceGatheringDelay => 30f;
        public override int ResourceGatheringAmount => 2;
        public override int ResourceGatheringDistance => 3;

        public Village(Tile tile, Player owner) : base(tile, owner) 
        { 
            AllowMovablesThrough = true;

            inventory = new Inventory<Resource>(GameSettings.VillagesInventorySlotsCount, GameSettings.VillagesInventoryStackSize);
        }

        private Dictionary<Resource, int> price = new Dictionary<Resource, int>()
        {
            {Resource.Wood, 2},
            {Resource.Clay, 1},
            {Resource.Stone, 0},
            {Resource.Wheat, 1}
        };
        public override Dictionary<Resource, int> BuildingPrice => price;

        public override string ToString() => "Village";
    }
}