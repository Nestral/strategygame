﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourcesDisplay : MonoBehaviour
{
    [SerializeField] private Text[] resourcesTexts;

    public void SetResourceCount(Resource resource, int count)
    {
        if (resourcesTexts.Length <= (int)resource)
            return;

        resourcesTexts[(int)resource].text = count.ToString();
    }

    public void SetResourceCount(Dictionary<Resource, int> resources)
    {
        foreach (KeyValuePair<Resource, int> KeyValuePair in resources)
        {
            if (resourcesTexts.Length <= (int)KeyValuePair.Key)
                continue;

            resourcesTexts[(int)KeyValuePair.Key].text = KeyValuePair.Value.ToString();
        }
    }
}
