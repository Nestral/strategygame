﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float zoomSpeed = 5f;
    public float minZoom = 1f;
    public float maxZoom = 10f;

    private Vector3 defaultPosition;
    private float defaultZoom;

    private bool canControl = false;
    private Vector2 prevMousePosition;

    private Camera Camera;

    private void Awake()
    {
        Camera = GetComponent<Camera>();
        defaultPosition = Camera.transform.position;
        defaultZoom = Camera.orthographicSize;
    }
     
    private void Start()
    {
        NetworkManager.instance.OnGameStarted += OnGameStart;
    }

    private void OnDisable()
    {
        NetworkManager.instance.OnGameEnded -= OnGameFinish;
    }

    private void OnGameStart()
    {
        canControl = true;
    }

    private void OnGameFinish()
    {
        // Reset to default position and zoom
        Camera.orthographicSize = defaultZoom;
        transform.position = defaultPosition;
        canControl = false;
    }

    private void FixedUpdate()
    {
        // Return if camera control is unavailable (i.e. in the main menu)
        if (!canControl)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            prevMousePosition = Input.mousePosition;
            return;
        }

        // Move camera
        Vector2 mousePosition = Input.mousePosition;
        if (Input.GetMouseButton(0))
        {
            Vector2 cameraMove = Camera.ScreenToWorldPoint(prevMousePosition) - Camera.ScreenToWorldPoint(mousePosition);

            transform.Translate(cameraMove);
        }
        prevMousePosition = mousePosition;

        // Zoom in/out
        Camera.orthographicSize = Mathf.Clamp(Camera.orthographicSize - Input.mouseScrollDelta.y * zoomSpeed * Time.deltaTime, minZoom, maxZoom);
    }
}
