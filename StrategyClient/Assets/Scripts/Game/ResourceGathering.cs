﻿using System.Collections.Generic;
using UnityEngine;

public abstract class ResourceGathering : Building
{
    public abstract float ResourceGatheringDelay { get; }
    public abstract int ResourceGatheringAmount { get; }
    public abstract int ResourceGatheringDistance { get; }
    public Tile ResourceGatheringTile { get; protected set; }

    public float TimeUntilGathering { get; private set; }

    public Inventory<Resource> inventory;

    public Dictionary<Resource, int> Resources => inventory?.collection;
    public bool HasInventory => inventory?.SlotsCount > 0;
    public int InventorySlotsCount => inventory == null ? 0 : inventory.SlotsCount;
    public int InventoryStackSize => inventory == null ? 0 : inventory.StackSize;
    public int InventoryStacksCount => inventory == null ? 0 : inventory.StacksCount;

    public void LoadResource(Resource resource, int count, out int extra)
    {
        inventory.LoadItem(resource, count, out extra);
        if (Game.SelectedBuilding == this && inventory.TryGetItemCount(resource, out int value))
            UIManager.instance.SetUIResources(UIManager.instance.buildingUIResourcesDisplay, resource, value);
    }
    public bool UnloadResource(Resource resource, int count)
    {
        if (!inventory.UnloadItem(resource, count))
            return false;

        if (Game.SelectedBuilding == this && inventory.TryGetItemCount(resource, out int value))
            UIManager.instance.SetUIResources(UIManager.instance.buildingUIResourcesDisplay, resource, value);

        return true;
    }
    public bool HasResources(Dictionary<Resource, int> resources) => inventory.Contains(resources);
    public void SetResources(Dictionary<Resource, int> resources)
    {
        inventory.SetCollection(resources);
        if (Game.SelectedBuilding == this)
            UIManager.instance.SetUIResources(UIManager.instance.buildingUIResourcesDisplay, resources);
    }

    public bool SetGatheringTile(Tile tile)
    {
        if (Tile.Distance(tile) <= ResourceGatheringDistance)
        {
            ResourceGatheringTile = tile;
            return true;
        }

        return false;
    }

    private void Update()
    {
        if (ResourceGatheringTile == null)
        {
            TimeUntilGathering = ResourceGatheringDelay;
            return;
        }

        TimeUntilGathering -= Time.deltaTime;
        if (TimeUntilGathering <= 0)
            GatherResources();
    }

    private void GatherResources()
    {
        if (ResourceGatheringTile.Resource == Resource.None)
            return;

        LoadResource(ResourceGatheringTile.Resource, ResourceGatheringAmount, out int _);
        TimeUntilGathering += ResourceGatheringDelay;
    }
}
