﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class Building : MonoBehaviour
{
    public Tile Tile { get; private set; }
    public Player Owner { get; private set; }
    public bool IsSelected { get; protected set; }
    public bool AllowMovablesThrough { get; protected set; }

    public List<Tile> connectedTiles;
    public List<Building> ConnectedBuildings => connectedTiles.Where(tile => tile.Building != null).Select(tile => tile.Building).ToList();

    public RoadSystem RoadSystem { get; set; }

    public abstract Dictionary<Resource, int> BuildingPrice { get; }

    public abstract void SetColor(Color color);

    public Game Game => Game.instance;
    public GameSettings GameSettings => Game.instance.gameSettings;

    public virtual void Select()
    {
        IsSelected = true;

        UIManager.instance.EnableUI(UIManager.instance.buildingUI);
    }

    public virtual void Deselect()
    {
        IsSelected = false;

        UIManager.instance.DisableUI(UIManager.instance.buildingUI);
    }

    public virtual void Init(Tile tile, Player owner, Color color)
    {
        Tile = tile;
        Owner = owner;
        SetColor(color);

        connectedTiles = new List<Tile>();

        // Connect near roads to the building
        foreach (Tile connectedTile in Tile.GetNeighbourTiles())
            if (connectedTile.Building != null && connectedTile.Building.AllowMovablesThrough)
                connectedTile.Building.ConnectTile(Tile);
    }

    public bool ConnectTile(Tile connectTile)
    {
        if (Tile.GetNeighbourTiles().Contains(connectTile))
        {
            connectedTiles.Add(connectTile);

            // Spawn visual connection
            ConnectVisual(connectTile);
            return true;
        }

        return false;
    }
    protected abstract void ConnectVisual(Tile connectTile);

    public void ConnectToRoadSystem()
    {
        // Connect to a nearby RoadSystem if possible
        bool connected = false;
        foreach (Tile connectedTile in Tile.GetNeighbourTiles())
        {
            if (connectedTile.Building != null && connectedTile.Building.AllowMovablesThrough)
            {
                // Connect to a road
                ConnectTile(connectedTile);

                // If we have not connected to a road system yet, then connect building to the road system
                if (!connected)
                {
                    connectedTile.Building.RoadSystem.AddBuilding(this);
                    connected = true;
                }
                // Otherwise connect two road systems
                else if (connectedTile.Building.RoadSystem != RoadSystem)
                    connectedTile.Building.RoadSystem.AddRoadSystem(this);
            }
        }

        if (!connected)
            RoadSystem = new RoadSystem(this);
    }
}
