﻿using System.Linq;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class PathVisual : MonoBehaviour
{
    private LineRenderer lineRenderer;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    public void VisualizePath(Tile[] path)
    {
        lineRenderer.enabled = true;
        lineRenderer.positionCount = path.Length;
        lineRenderer.SetPositions(path.Select(tile => new Vector3(tile.transform.position.x, tile.transform.position.y, -5)).ToArray());
    }

    public void HidePath()
    {
        lineRenderer.positionCount = 0;
        lineRenderer.enabled = false;
    }

    public void SetColors(Color startColor, Color endColor)
    {
        lineRenderer.startColor = startColor;
        lineRenderer.endColor = endColor;
    }
}
