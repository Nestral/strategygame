﻿using System.Collections.Generic;
using UnityEngine;

public class Village : ResourceGathering
{
    public override float ResourceGatheringDelay => 30f;
    public override int ResourceGatheringAmount => 2;
    public override int ResourceGatheringDistance => 3;

    public override void Init(Tile tile, Player owner, Color color)
    {
        base.Init(tile, owner, color);
        AllowMovablesThrough = true;

        inventory = new Inventory<Resource>(GameSettings.VillagesInventorySlotsCount, GameSettings.VillagesInventoryStackSize);
    }

    public override void SetColor(Color color)
    {
        GetComponent<SpriteRenderer>().color = color;
    }

    protected override void ConnectVisual(Tile connectTile)
    {

    }

    private Dictionary<Resource, int> price = new Dictionary<Resource, int>()
        {
            {Resource.Wood, 2},
            {Resource.Clay, 1},
            {Resource.Stone, 0},
            {Resource.Wheat, 1}
        };
    public override Dictionary<Resource, int> BuildingPrice => price;
}
