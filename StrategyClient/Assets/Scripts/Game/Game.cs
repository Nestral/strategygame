﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GameSettings), typeof(Map))]
public class Game : MonoBehaviour
{
    #region Singleton
    public static Game instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.LogWarning("Instance already exists, destroying object!");
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    #endregion

    public Map map;
    public GameSettings gameSettings;

    public Tile SelectedTile { get; private set; }
    public Building SelectedBuilding { get; private set; }
    public Movable SelectedMovable { get; private set; }
    public GameObject PlanningBuild { get; private set; }

    private Dictionary<int, Movable> movables;
    private Dictionary<int, Player> players;

    private void Start()
    {
        if (map == null)
            map = GetComponent<Map>();
        if (gameSettings == null)
            gameSettings = GetComponent<GameSettings>();
    }

    internal void StartGame(Dictionary<int, Player> players)
    {
        this.players = players;
        movables = new Dictionary<int, Movable>();
    }

    internal void EndGame()
    {

    }

    public bool TryGetMovable(int movableId, out Movable movable) => movables.TryGetValue(movableId, out movable);

    internal void SelectTile(Tile tile)
    {
        // Make building gather resources from that tile
        if (SelectedBuilding is ResourceGathering resourceGathering)
        {
            ClientSend.SetBuildingGatheringTileRequest(resourceGathering, tile);

            SelectedBuilding?.Deselect();
            SelectedTile?.Deselect();

            SelectedBuilding = null;
            SelectedTile = null;
            return;
        }

        // Make movable move towards that tile    
        if (SelectedMovable != null)
        {
            ClientSend.SetMovableTargetTileRequest(SelectedMovable, tile);

            SelectedMovable?.Deselect();
            SelectedTile?.Deselect();

            SelectedMovable = null;
            SelectedTile = null;
            return;
        }

        SelectedMovable?.Deselect();
        SelectedBuilding?.Deselect();
        SelectedTile?.Deselect();

        // Select tile
        SelectedTile = tile;
        SelectedTile.Select();

        Debug.Log($"Tile(X:{tile.Position.x}, Y:{tile.Position.y}) selected.");
    }

    internal void SelectBuilding(Building building)
    {
        SelectedMovable?.Deselect();
        SelectedBuilding?.Deselect();
        SelectedTile?.Deselect();

        SelectedMovable = null;
        SelectedBuilding = building;
        building.Select();

        Debug.Log($"Building on tile(X:{building.Tile.Position.x}, Y:{building.Tile.Position.y}) selected.");
    }

    internal void SelectMovable(Movable movable)
    {
        SelectedMovable?.Deselect();
        SelectedBuilding?.Deselect();
        SelectedTile?.Deselect();

        SelectedBuilding = null;
        SelectedMovable = movable;
        movable.Select();

        Debug.Log($"Movable(ID:{movable.MovableId}) selected.");
    }

    internal void HoverTile(Tile tile)
    {
        tile.ShowOutline();
    }

    internal void UnhoverTile(Tile tile)
    {
        tile.HideOutline();
    }

    public void PlanVillage()
    {
        PlanningBuild = map.villagePrefab.gameObject;

        Debug.Log($"Village planned.");
    }

    public void PlanRoad()
    {
        PlanningBuild = map.roadPrefab.gameObject;

        Debug.Log($"Road planned.");
    }

    public void PlanCaravan()
    {
        PlanningBuild = map.caravanPrefab.gameObject;

        Debug.Log($"Caravan planned.");
    }

    public void BuildPlanRequest()
    {
        if (SelectedTile == null)
            return;

        if (PlanningBuild == map.roadPrefab.gameObject)
            ClientSend.BuildRoadRequest(SelectedTile);
        else if (PlanningBuild == map.villagePrefab.gameObject)
            ClientSend.BuildVillageRequest(SelectedTile);
        else if (PlanningBuild == map.caravanPrefab.gameObject)
            ClientSend.BuildCaravanRequest(SelectedTile);
    }

    public void SpawnWarriorRequest()
    {
        if (SelectedMovable != null)
            ClientSend.SpawnWarriorRequest(SelectedMovable, 1);
    }

    public void LoadResourcesToMovableRequest()
    {
        if (SelectedMovable != null)
            ClientSend.LoadResourcesToMovable(SelectedMovable);
    }

    public void UnloadResourcesFromMovableRequest()
    {
        if (SelectedMovable != null)
            ClientSend.UnloadResourcesFromMovable(SelectedMovable);
    }

    public void BuildVillage(int id, int x, int y)
    {
        if (!map.TryGetTile(new Vector2Int(x, y), out Tile tile))
            return;

        Building village = tile.Build(map.villagePrefab, players[id]);
    }

    public void BuildRoad(int playerId, int roadX, int roadY)
    {
        // Return if no such tiles exist
        if (!map.TryGetTile(new Vector2Int(roadX, roadY), out Tile roadTile))
            return;

        // Build road
        Road road = roadTile.Build(map.roadPrefab, players[playerId]).GetComponent<Road>();
    }

    internal void BuildCaravan(int playerId, int tileX, int tileY, int movableId)
    {
        // Return if no such tile exists or no ResourceGathering Building there
        if (!map.TryGetTile(new Vector2Int(tileX, tileY), out Tile tile)
            || tile.Building == null
            || !(tile.Building is Road))
            return;

        Caravan caravan = tile.BuildMovable(map.caravanPrefab, players[playerId], movableId).GetComponent<Caravan>();
        movables.Add(movableId, caravan);
    }

    internal void SetMovableTargetTile(int movableId, int tileX, int tileY)
    {
        if (!map.TryGetTile(new Vector2Int(tileX, tileY), out Tile tile)
            // || tile.Building == null
            || !movables.TryGetValue(movableId, out Movable movable))
            return;

        movable.SetDestinationTile(tile);
    }

    internal void MovableMoved(int movableId, int tileX, int tileY)
    {
        if (!map.TryGetTile(new Vector2Int(tileX, tileY), out Tile tile)
            || !movables.TryGetValue(movableId, out Movable movable))
            return;

        movable.CheckLocation(tile);
    }

    internal bool SpawnWarrior(int fromClient, int movableId, int warriorStrength)
    {
        return (movables.TryGetValue(movableId, out Movable movable)
            && movable.SpawnWarrior(warriorStrength));
    }

    internal bool LoadResourcesToMovable(int movableId, Dictionary<Resource, int> resources)
    {
        return (movables.TryGetValue(movableId, out Movable movable)
            && movable.TryLoadResourcesFromBuilding(resources));
    }

    internal bool UnloadResourcesFromMovable(int movableId, Dictionary<Resource, int> resources)
    {
        return (movables.TryGetValue(movableId, out Movable movable)
            && movable.TryUnloadResourcesToBuilding(resources));
    }

    internal void UpdateMovableInventory(int movableId, Dictionary<Resource, int> resources)
    {
        if (!movables.TryGetValue(movableId, out Movable movable))
            return;

        movable.SetResources(resources);
    }

    internal bool RGBuildingResourcesChange(int tileX, int tileY, Dictionary<Resource, int> resources)
    {
        if (!map.TryGetTile(new Vector2Int(tileX, tileY), out Tile tile)
            || tile.Building == null || !(tile.Building is ResourceGathering rgBuilding))
            return false;
        
        rgBuilding.SetResources(resources);
        return true;
    }
}
