﻿using System.Collections.Generic;
using UnityEngine;

public class Road : Building
{
    public GameObject RoadPartPrefab;

    public override void Init(Tile tile, Player owner, Color color)
    {
        base.Init(tile, owner, color);
        AllowMovablesThrough = true;
    }

    public override void SetColor(Color color)
    {
        foreach (SpriteRenderer spriteRenderer in GetComponentsInChildren<SpriteRenderer>())
        {
            spriteRenderer.color = color;
        }
    }

    protected override void ConnectVisual(Tile connectTile)
    {
        Vector3 position = Tile.transform.position;
        Quaternion rotation = Quaternion.Euler(0, 0, Vector2.SignedAngle(Vector2.right, (Vector2)(connectTile.transform.position - Tile.transform.position)));
        GameObject part = Instantiate(RoadPartPrefab, position, rotation, transform);
        part.GetComponent<SpriteRenderer>().color = Owner.Color;
    }

    private Dictionary<Resource, int> price = new Dictionary<Resource, int>()
        {
            {Resource.Wood, 1},
            {Resource.Clay, 1},
            {Resource.Stone, 0},
            {Resource.Wheat, 0}
        };
    public override Dictionary<Resource, int> BuildingPrice => price;

    public override string ToString() => "Road";

}
