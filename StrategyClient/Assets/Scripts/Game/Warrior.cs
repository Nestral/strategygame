﻿using UnityEngine;

public class Warrior : MonoBehaviour
{
    public int Strength { get; private set; }

        public void Init(int strength)
        {
            Strength = strength;
        }
}
