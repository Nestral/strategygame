﻿using System.Collections.Generic;
using UnityEngine;

public abstract class Movable : MonoBehaviour
{
    [SerializeField] private PathVisual pathVisual;

    public Player Owner { get; protected set; }
    public bool IsSelected { get; protected set; }

    public int MovableId { get; private set; }

    public List<Warrior> warriors;
    public int WarriorsPower { get; private set; }

    // Movement speed in Tiles/second
    public abstract float MoveSpeed { get; }

    public Tile CurrentTile { get; protected set; }
    protected float currentTileMovingProgress;
    public Tile DestinationTile { get; private set; }
    protected int currentTileIndex;
    protected Tile[] path;

    public Inventory<Resource> inventory;

    protected Tile positionRelatedTile;

    public abstract Dictionary<Resource, int> BuildingPrice { get; }

    public Game Game => Game.instance;
    public GameSettings GameSettings => Game.instance.gameSettings;

    public virtual void Init(Tile tile, Player owner, int movableId)
    {
        MovableId = movableId;
        CurrentTile = tile;
        Owner = owner;

        positionRelatedTile = CurrentTile;
        currentTileMovingProgress = 0.5f;
    }

    public virtual void Select()
    {
        IsSelected = true;
        if (path != null)
            pathVisual.VisualizePath(path);

        UIManager.instance.EnableUI(UIManager.instance.movableUI);
        UIManager.instance.SetUIResources(UIManager.instance.movableUIResourcesDisplay, Resources);
    }

    public virtual void Deselect()
    {
        IsSelected = false;
        pathVisual.HidePath();

        UIManager.instance.DisableUI(UIManager.instance.movableUI);
    }

    public bool HasInventory => inventory?.SlotsCount > 0;
    public int InventorySlotsCount => inventory == null ? 0 : inventory.SlotsCount;
    public int InventoryStackSize => inventory == null ? 0 : inventory.StackSize;
    public int InventoryStacksCount => inventory == null ? 0 : inventory.StacksCount;

    public Dictionary<Resource, int> Resources => inventory?.collection;

    public void LoadResource(Resource resource, int count, out int extra)
    {
        inventory.LoadItem(resource, count, out extra);
        if (Game.SelectedMovable == this && inventory.TryGetItemCount(resource, out int value))
            UIManager.instance.SetUIResources(UIManager.instance.movableUIResourcesDisplay, resource, value);
    }
    public bool UnloadResource(Resource resource, int count)
    {
        if (!inventory.UnloadItem(resource, count))
            return false;

        if (Game.SelectedMovable == this && inventory.TryGetItemCount(resource, out int value))
            UIManager.instance.SetUIResources(UIManager.instance.movableUIResourcesDisplay, resource, value);

        return true;
    }
    public bool HasResources(Dictionary<Resource, int> resources) => inventory.Contains(resources);
    public void SetResources(Dictionary<Resource, int> resources)
    {
        inventory.SetCollection(resources);
        if (Game.SelectedMovable == this)
            UIManager.instance.SetUIResources(UIManager.instance.movableUIResourcesDisplay, resources);
    }

    public bool TryLoadResourcesFromBuilding(Dictionary<Resource, int> resources)
    {
        // Check if movable is in the building, that has enough resources
        if (CurrentTile.Building == null || !(CurrentTile.Building is ResourceGathering building)
            || !building.HasResources(resources))
            return false;

        // Load resources
        foreach (KeyValuePair<Resource, int> keyValuePair in resources)
        {
            LoadResource(keyValuePair.Key, keyValuePair.Value, out int extra);
            building.UnloadResource(keyValuePair.Key, keyValuePair.Value - extra);
        }
        return true;
    }

    public bool TryUnloadResourcesToBuilding(Dictionary<Resource, int> resources)
    {
        // Check if movable is in the building, and movable has enough resources
        if (CurrentTile.Building == null || !(CurrentTile.Building is ResourceGathering building)
            || !HasResources(resources))
            return false;

        // Unload resources
        foreach (KeyValuePair<Resource, int> keyValuePair in resources)
        {
            building.LoadResource(keyValuePair.Key, keyValuePair.Value, out int extra);
            UnloadResource(keyValuePair.Key, keyValuePair.Value - extra);
        }
        return true;
    }

    public bool SetDestinationTile(Tile tile)
    {
        DestinationTile = tile;

        // Remember previous and next tiles on the path for later
        Tile prevTile = (path == null || currentTileIndex == 0) ? null : path[currentTileIndex - 1];
        Tile nextTile = (path == null || path.Length <= currentTileIndex + 1) ? null : path[currentTileIndex + 1];

        // Calculate path if tile is not null, otherwise set path to null
        path = tile == null ? null : FindPath(DestinationTile);

        // Change currentTileMovingProgress value, if needed
        if (path != null && path.Length >= 2
            && (currentTileMovingProgress < 0.5f && prevTile != null && prevTile == path[1]
            || currentTileMovingProgress > 0.5f && nextTile != null && nextTile != path[1]))
        {
            currentTileMovingProgress = 1 - currentTileMovingProgress;
        }

        return true;
    }

    private void Update()
    {
        if (DestinationTile != null)
            MoveTowardsTile(DestinationTile);
    }

    protected abstract bool IsTileAccesible(Tile tile);
    protected abstract Tile[] FindPath(Tile toTile);

    protected void MoveTowardsTile(Tile tile)
    {
        if (path == null || tile == CurrentTile && currentTileMovingProgress >= 0.5f)
            return;

        currentTileMovingProgress += MoveSpeed * Time.deltaTime;
        if (currentTileMovingProgress >= 1)
        {
            currentTileMovingProgress--;
            MoveTileForward(tile);
        }

        // Calculate position for the movable
        if (currentTileMovingProgress < 0.5)
        {
            if (currentTileIndex > 0)
                positionRelatedTile = path[currentTileIndex - 1];
            Vector3 pos2 = CurrentTile.transform.position;
            Vector3 pos1 = (positionRelatedTile.transform.position + pos2) / 2;
            transform.position = pos1 + (pos2 - pos1) * currentTileMovingProgress * 2;
        }
        else if (currentTileMovingProgress > 0.5 && path.Length > currentTileIndex + 1)
        {
            positionRelatedTile = path[currentTileIndex + 1];
            Vector3 pos1 = CurrentTile.transform.position;
            Vector3 pos2 = (positionRelatedTile.transform.position + pos1) / 2;
            transform.position = pos1 + (pos2 - pos1) * (currentTileMovingProgress - 0.5f) * 2;
        }
        else
        {
            transform.position = CurrentTile.transform.position;
        }
    }

    protected void MoveTileForward(Tile tile)
    {
        if (path.Length <= currentTileIndex + 1)
            return;

        currentTileIndex++;
        CurrentTile = path[currentTileIndex];
        currentTileMovingProgress = 0f;
    }

    internal void CheckLocation(Tile tile)
    {
        // Change location if it is wrong and not close
        if (CurrentTile != tile && (currentTileMovingProgress <= 0.95f || path[currentTileIndex + 1] != tile))
        {
            CurrentTile = tile;
            currentTileMovingProgress = 0;
            transform.position = (CurrentTile.transform.position
                + (currentTileIndex > 0 ? path[currentTileIndex - 1].transform.position : CurrentTile.transform.position)) / 2;

            // Update path
            path = FindPath(DestinationTile);
            Debug.Log("Movable's(ID:{MovableId}) location corrected.");
        }
    }

    internal bool SpawnWarrior(int warriorStrength)
    {
        Warrior warrior = Instantiate(Game.instance.map.warriorPrefab, transform.position, Quaternion.identity, transform).GetComponent<Warrior>();
        warrior.Init(warriorStrength);
        warriors.Add(warrior);
        WarriorsPower += warriorStrength;
        return true;
    }

    private void OnMouseDown()
    {
        Game.instance.SelectMovable(this);
    }
}
