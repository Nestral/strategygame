﻿using System.Collections.Generic;
using UnityEngine;

public class Player
{
    public static Player localPlayer;

    public int ID { get; private set; }
    public string Username { get; private set; }
    public bool Ready { get; private set; }
    public Color Color { get; private set; }

    public Player(int id, string username, Color color)
    {
        ID = id;
        Username = username;
        Color = color;

        if (id == Client.instance.id)
            localPlayer = this;
    }

    public void SetReady(bool ready)
    {
        Ready = ready;
    }
}
