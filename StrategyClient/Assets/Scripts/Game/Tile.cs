﻿using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [SerializeField] private SpriteRenderer tileOutline;

    public bool IsSelected { get; private set; }

    public Resource Resource { get; private set; }
    public Biome Biome { get; private set; }
    public Vector2Int Position { get; private set; }

    public Building Building { get; private set; }

    public void Select()
    {
        IsSelected = true;

        UIManager.instance.EnableUI(UIManager.instance.tileUI);
    }

    public void Deselect()
    {
        IsSelected = false;

        UIManager.instance.DisableUI(UIManager.instance.tileUI);
    }

    public void Initialize(Vector2Int position, Biome biome)
    {
        Resource = BiomesResource[biome];
        Biome = biome;
        Position = position;
    }

    public Building Build(Building buildingPrefab, Player owner)
    {
        if (Building == null)
        {
            Building = Instantiate(buildingPrefab, transform.position, Quaternion.identity, transform).GetComponent<Building>();
            Building.Init(this, owner, owner.Color);
            Building.ConnectToRoadSystem();
            TryPayResources(Building.BuildingPrice, owner);

            return Building;
        }

        return null;
    }

    public Movable BuildMovable(Movable movablePrefab, Player owner, int movableId)
    {
        Movable movable = Instantiate(movablePrefab, transform.position, Quaternion.identity, Game.instance.transform).GetComponent<Movable>();
        movable.Init(this, owner, movableId);
        movable.GetComponent<SpriteRenderer>().color = owner.Color;
        TryPayResources(movable.BuildingPrice, owner);

        return movable;
    }

    private bool TryPayResources(Dictionary<Resource, int> buildingPrice, Player player)
    {
        return true;
    }

    public IEnumerable<Tile> GetNeighbourTiles()
    {
        Vector2Int[] neigbourPositions = GetNeighbourTilesPositions();
        foreach (Vector2Int position in neigbourPositions)
        {
            if (Game.instance.map.TryGetTile(position, out Tile tile))
                yield return tile;
        }
    }

    public Vector2Int[] GetNeighbourTilesPositions() => new Vector2Int[]
    {
            new Vector2Int(Position.x, Position.y + 1),
            new Vector2Int(Position.x + 1, Position.y),
            new Vector2Int(Position.x + 1, Position.y - 1),
            new Vector2Int(Position.x, Position.y - 1),
            new Vector2Int(Position.x - 1, Position.y),
            new Vector2Int(Position.x - 1, Position.y + 1)
    };

    public int Distance(Tile targetTile)
    {
        if (targetTile == this)
            return 0;

        Dictionary<Vector2Int, int> tilesDist = new Dictionary<Vector2Int, int>();
        List<Tile> nextTiles = new List<Tile>();

        nextTiles.Add(this);
        tilesDist.Add(Position, 0);

        return CalculateDistance(targetTile, 0, nextTiles, tilesDist);
    }

    private int CalculateDistance(Tile targetTile, int currentDistance, List<Tile> nextTiles, Dictionary<Vector2Int, int> tilesDist)
    {
        List<Tile> _nextTiles = new List<Tile>();
        foreach (Tile tile in nextTiles)
        {
            foreach (Tile neighbour in tile.GetNeighbourTiles())
            {
                if (tilesDist.ContainsKey(neighbour.Position))
                    continue;

                if (neighbour == targetTile)
                    return currentDistance + 1;

                tilesDist.Add(neighbour.Position, currentDistance + 1);
                _nextTiles.Add(neighbour);
            }
        }

        return _nextTiles.Count == 0
            ? int.MaxValue
            : CalculateDistance(targetTile, currentDistance + 1, _nextTiles, tilesDist);
    }

    public static Dictionary<Biome, Resource> BiomesResource = new Dictionary<Biome, Resource>()
        {
            { Biome.Forest, Resource.Wood },
            { Biome.Plains, Resource.Wheat },
            { Biome.Mountains, Resource.Stone },
            { Biome.Desert, Resource.None },
            { Biome.River, Resource.Clay },
            { Biome.Lake, Resource.Clay },
            { Biome.Ocean, Resource.None }
        };

    public void ShowOutline()
    {
        tileOutline.enabled = true;
    }

    public void HideOutline()
    {
        tileOutline.enabled = false;
    }

    private void OnMouseEnter()
    {
        Game.instance.HoverTile(this);
    }

    private void OnMouseExit()
    {
        Game.instance.UnhoverTile(this);
    }

    private void OnMouseDown()
    {
        Game.instance.SelectTile(this);
        if (Building != null)
            Game.instance.SelectBuilding(Building);
    }
}

public enum Biome
{
    Forest,
    Plains,
    Mountains,
    Desert,
    River,
    Lake,
    Ocean
}

public enum Resource
{
    Wood,
    Clay,
    Stone,
    Wheat,
    None
}
