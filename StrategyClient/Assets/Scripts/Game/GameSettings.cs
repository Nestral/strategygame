using UnityEngine;

public class GameSettings : MonoBehaviour
{
    public int CaravansInventorySlotsCount = 2;
    public int CaravansInventoryStackSize = 2;

    public int VillagesInventorySlotsCount = 5;
    public int VillagesInventoryStackSize = 10;
}