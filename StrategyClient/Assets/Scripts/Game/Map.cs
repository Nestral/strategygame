﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Map : MonoBehaviour
{
    [SerializeField] private List<GameObject> tilesPrefabs;
    [SerializeField] internal Village villagePrefab;
    [SerializeField] internal Road roadPrefab;
    [SerializeField] internal Caravan caravanPrefab;
    [SerializeField] internal Warrior warriorPrefab;

    private Dictionary<Biome, GameObject> tilesPrefabsDict = new Dictionary<Biome, GameObject>();

    [SerializeField] private GameObject tilesHolder;

    private Dictionary<Vector2Int, Tile> tiles = new Dictionary<Vector2Int, Tile>();

    private void Start()
    {
        if (tilesPrefabs.Count != Enum.GetValues(typeof(Biome)).Cast<int>().Count())
        {
            throw new ArgumentException("Too much or not enough tile prefabs");
        }

        tilesPrefabsDict.Clear();
        for (int i = 0; i < tilesPrefabs.Count; i++)
        {
            tilesPrefabsDict.Add((Biome)i, tilesPrefabs[i]);
        }
    }

    internal void ResetMap()
    {
        foreach (Vector2Int position in tiles.Keys)
        {
            DestroyTile(position);
        }
    }

    public bool TryGetTile(Vector2Int position, out Tile tile)
    {
        return tiles.TryGetValue(position, out tile);
    }

    public void ReplaceTile(Vector2Int position, Biome replaceBiome)
    {
        DestroyTile(position);
        SpawnTile(position, replaceBiome);
    }

    public void DestroyTile(Vector2Int position)
    {
        Tile destroyTile = tiles[position];
        Destroy(destroyTile.gameObject);
        tiles.Remove(position);
    }

    public void SpawnTile(Vector2Int position, Biome biome)
    {
        Tile tile = Instantiate(tilesPrefabs[(int)biome], MapToWorldPosition(position), Quaternion.identity, tilesHolder.transform).GetComponent<Tile>();
        tile.Initialize(position, biome);
        tiles.Add(position, tile);
    }

    private const float MAP_X_TO_WORLD_X = 0.866f; //Math.Sqrt(3) / 2
    private const float MAP_X_TO_WORLD_Y = 0.5f;
    private const float MAP_Y_TO_WORLD_Y = 1f;
    private const float MAP_Y_TO_WORLD_X = 0f;
    public Vector2 MapToWorldPosition(Vector2Int mapPosition)
    {
        return new Vector2(
            mapPosition.x * MAP_X_TO_WORLD_X + mapPosition.y * MAP_Y_TO_WORLD_X,
            mapPosition.x * MAP_X_TO_WORLD_Y + mapPosition.y * MAP_Y_TO_WORLD_Y);
    }

}
