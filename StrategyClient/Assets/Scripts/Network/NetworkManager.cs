﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class NetworkManager : MonoBehaviour
{
    #region Singleton
    public static NetworkManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.LogWarning("Instance already exists, destroying object!");
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    #endregion

    private Dictionary<int, ServerRoom> rooms = new Dictionary<int, ServerRoom>();
    //private Dictionary<int, RoomManager> playerRooms = new Dictionary<int, RoomManager>();

    private RoomManager myRoom;

    internal ServerRoom GetRoom(int gameId)
    {
        return rooms[gameId];
    }

    public void ConnectToServer()
    {
        ConnectToServer(UIManager.instance.GetServerAddressString());
    }

    internal void ConnectToServer(string serverAddress)
    {
        Client.instance.username = UIManager.instance.GetUsername();
        if (Client.instance.username.Trim().Length == 0)
            return;

        IPAddress serverIP;
        if (serverAddress == "")
        {
            Debug.Log("Connecting to local host...");
            serverIP = IPAddress.Parse("127.0.0.1");
        }
        else
        {
            IPHostEntry serverHostEntry;
            try
            {
                serverHostEntry = Dns.GetHostEntry(serverAddress);
            }
            catch
            {
                Debug.Log("Unable to find host with specified name or address.");
                return;
            }

            Debug.Log($"Found host {serverHostEntry.HostName}. Connecting...");
            serverIP = serverHostEntry.AddressList[0];
        }

        Client.instance.ConnectToServer(serverIP);
    }

    public void ConnectToRoom()
    {
        if (int.TryParse(UIManager.instance.GetRoomIdString(), out int roomId))
        {
            ConnectToRoom(roomId);
        }
    }

    internal void ConnectToRoom(int roomId)
    {
        ClientSend.ConnectToRoomRequest(roomId);
    }

    public void CreateNewRoom()
    {
        ClientSend.MakeNewRoomRequest();
    }

    public void DisconnectFromServer()
    {
        Client.instance.Disconnect();
        //OnPlayerDisconnectedFromServer(Client.instance.id);
    }

    public void DisconnectFromRoom()
    {
        ClientSend.LeaveRoomRequest();
    }

    public void ChangeReadyState()
    {
        ClientSend.ChangeReadyStateRequest();
    }

    #region Events
    public event Action OnPlayerConnectedToServer;

    internal void PlayerConnectedToServer(int playerId)
    {
        UIManager.instance.OnConnectedToServer();

        OnPlayerConnectedToServer?.Invoke();
    }

    internal event Action OnPlayerDisconnectedFromServer;

    internal void PlayerDisconnectedFromServer(int playerId)
    {
        UIManager.instance.OnDisconnectedFromServer();

        OnPlayerDisconnectedFromServer?.Invoke();
    }

    public event Action<int> OnGameHosted;

    internal void GameHosted(int roomId)
    {
        rooms.Clear();
        RoomManager room = new RoomManager(roomId);
        //rooms.Add(roomId, room);
        //playerRooms.Add(Client.instance.id, room);

        myRoom = room;
        myRoom.PlayerJoin(Client.instance.id, Client.instance.username);

        UIManager.instance.OnHostGame();

        OnGameHosted?.Invoke(roomId);
    }

    public event Action OnConnectedToRoom;

    internal void ConnectedToRoom(RoomManager room)
    {
        myRoom = room;
        UIManager.instance.OnConnectedToRoom(myRoom);

        OnConnectedToRoom?.Invoke();
    }

    public event Action<int, string> OnPlayerConnectedToRoom;

    internal void PlayerConnectedToRoom(int playerId, string username)
    {
        if (myRoom == null)
            return;

        //playerRooms.Add(playerId, myRoom);
        myRoom.PlayerJoin(playerId, username);

        UIManager.instance.OnPlayerConnectedToRoom(playerId, username);

        OnPlayerConnectedToRoom?.Invoke(playerId, username);
    }

    public event Action<int> OnPlayerDisconnectedFromRoom;

    internal void PlayerDisconnectedFromRoom(int playerId)
    {
        if (myRoom == null)
            return;

        //playerRooms.Remove(playerId);
        myRoom.PlayerLeave(playerId);

        UIManager.instance.OnPlayerDisconnectedFromRoom(playerId);

        OnPlayerDisconnectedFromRoom?.Invoke(playerId);
    }

    public event Action<int, bool> OnPlayerReadyStateChanged;

    internal void PlayerReadyStateChanged(int playerId, bool ready)
    {
        if (myRoom == null)
            return;

        myRoom.PlayerReady(playerId, ready);

        UIManager.instance.OnPlayerReady(playerId, ready);

        OnPlayerReadyStateChanged?.Invoke(playerId, ready);
    }

    public event Action<Dictionary<int, ServerRoom>> OnRoomsStateReceived;

    internal void RoomsStateReceived(Dictionary<int, ServerRoom> rooms)
    {
        this.rooms = rooms;

        OnRoomsStateReceived?.Invoke(rooms);
    }

    public event Action OnGameStarted;

    internal void GameStarted()
    {
        if (myRoom == null)
            return;

        myRoom.StartGame();

        UIManager.instance.OnGameStarted();

        OnGameStarted?.Invoke();
    }

    public event Action OnGameEnded;

    internal void GameEnded()
    {
        if (myRoom == null)
            return;

        myRoom.EndGame();

        UIManager.instance.OnGameEnded();

        OnGameEnded?.Invoke();
    }
    #endregion

}
