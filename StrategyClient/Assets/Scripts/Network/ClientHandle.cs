﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ClientHandle
{
    public static void Welcome(Packet packet)
    {
        Client.instance.isConnected = true;

        string msg = packet.ReadString();
        int myId = packet.ReadInt();

        //Connected to server successfully
        Debug.Log("Connected to server successfully.");

        Client.instance.id = myId;
        ClientSend.WelcomeReceived(Client.instance.username);

        NetworkManager.instance.PlayerConnectedToServer(myId);
    }

    public static void RoomsStateReceived(Packet packet)
    {
        Dictionary<int, ServerRoom> rooms = new Dictionary<int, ServerRoom>();
        //Dictionary<int, RoomManager> playerRooms = new Dictionary<int, RoomManager>();

        //Read rooms' current state
        int roomCount = packet.ReadInt();
        for (int i = 0; i < roomCount; i++)
        {
            int roomId = packet.ReadInt();
            int playersCount = packet.ReadInt();

            ServerRoom room = new ServerRoom(roomId, playersCount);
            rooms.Add(roomId, room);

            ////Players
            //for (int j = 0; j < playersCount; j++)
            //{
            //    //Player ID
            //    int player = packet.ReadInt();
            //    room.PlayerJoin(player, "");
            //    playerRooms.Add(player, room);

            //    //Player ready state
            //    bool ready = packet.ReadBool();
            //    room.PlayerReady(player, ready);

            //}
        }

        //Update
        NetworkManager.instance.RoomsStateReceived(rooms);
    }

    public static void RoomHosted(Packet packet)
    {
        int clientId = packet.ReadInt();
        int roomId = packet.ReadInt();

        if (Client.instance.id != clientId)
            return;

        NetworkManager.instance.GameHosted(roomId);
    }

    public static void RoomInfoReceived(Packet packet)
    {
        int roomId = packet.ReadInt();

        RoomManager room = new RoomManager(roomId);
        Dictionary<int, Player> players = new Dictionary<int, Player>();

        int playersCount = packet.ReadInt();
        for (int i = 0; i < playersCount; i++)
        {
            int playerId = packet.ReadInt();
            string playerName = packet.ReadString();
            bool playerReady = packet.ReadBool();

            Player newPlayer = new Player(playerId, playerName, UIManager.instance.playerColors[i]);
            newPlayer.SetReady(playerReady);
            players.Add(playerId, newPlayer);
        }

        room.SetPlayers(players);

        NetworkManager.instance.ConnectedToRoom(room);
    }

    public static void PlayerConnectedToRoom(Packet packet)
    {
        int clientId = packet.ReadInt();
        string username = packet.ReadString();

        if (clientId == Client.instance.id)
            return;
        else
            NetworkManager.instance.PlayerConnectedToRoom(clientId, username);

        Debug.Log($"Player (ID: {clientId}, Name: {username}) joined the room.");
    }

    public static void PlayerLeftRoom(Packet packet)
    {
        int playerId = packet.ReadInt();

        NetworkManager.instance.PlayerDisconnectedFromRoom(playerId);

        Debug.Log($"Player {playerId} left the room.");
    }

    public static void PlayerReadyStateChangeReceived(Packet packet)
    {
        int playerId = packet.ReadInt();
        bool ready = packet.ReadBool();

        NetworkManager.instance.PlayerReadyStateChanged(playerId, ready);

        Debug.Log($"Player {playerId} has changed his ready state.");
    }

    public static void GameStartReceived(Packet packet)
    {
        Game.instance.map.ResetMap();

        int tilesCount = packet.ReadInt();
        for (int i = 0; i < tilesCount; i++)
        {
            int x = packet.ReadInt();
            int y = packet.ReadInt();
            Biome biome = (Biome)packet.ReadInt();

            Game.instance.map.SpawnTile(new Vector2Int(x, y), biome);
        }

        NetworkManager.instance.GameStarted();

        Debug.Log($"The game has started.");
    }

    public static void GameEndReceived(Packet packet)
    {
        NetworkManager.instance.GameEnded();

        Debug.Log($"The game has ended.");
    }

    public static void VillageBuildReceived(Packet packet)
    {
        int id = packet.ReadInt();
        int x = packet.ReadInt();
        int y = packet.ReadInt();

        Game.instance.BuildVillage(id, x, y);
        //NetworkManager.instance.OnVillageBuilt();

        Debug.Log($"Village was built.");
    }

    public static void RoadBuildReceived(Packet packet)
    {
        int clientId = packet.ReadInt();
        int roadX = packet.ReadInt();
        int roadY = packet.ReadInt();
        // int fromTileX = packet.ReadInt();
        // int fromTileY = packet.ReadInt();
        // int toTileX = packet.ReadInt();
        // int toTileY = packet.ReadInt();

        Game.instance.BuildRoad(clientId, roadX, roadY);
        //NetworkManager.instance.OnRoadBuilt();

        Debug.Log($"Road was built.");
    }

    public static void CaravanBuildReceived(Packet packet)
    {
        int playerId = packet.ReadInt();
        int tileX = packet.ReadInt();
        int tileY = packet.ReadInt();
        int movableId = packet.ReadInt();

        Game.instance.BuildCaravan(playerId, tileX, tileY, movableId);

        Debug.Log($"Caravan was built.");
    }

    public static void BuildingGatheringTileSetReceived(Packet packet)
    {
        int id = packet.ReadInt();
        int buildX = packet.ReadInt();
        int buildY = packet.ReadInt();
        int gatherTileX = packet.ReadInt();
        int gatherTileY = packet.ReadInt();

        if (Game.instance.map.TryGetTile(new Vector2Int(buildX, buildY), out Tile buildTile)
            && buildTile.Building != null && buildTile.Building is ResourceGathering resourceGathering
            && Game.instance.map.TryGetTile(new Vector2Int(gatherTileX, gatherTileY), out Tile gatherTile))
        {
            resourceGathering.SetGatheringTile(gatherTile);
        }

        Debug.Log($"Buidling on tile(X:{buildX}, Y:{buildY}) is now gathering resources from tile(X:{gatherTileX},Y:{gatherTileY}).");
    }

    internal static void RGBuildingResourcesChangeReceived(Packet packet)
    {
        int tileX = packet.ReadInt();
        int tileY = packet.ReadInt();

        Dictionary<Resource, int> resources = new Dictionary<Resource, int>();
        int resourcesCount = packet.ReadInt();
        for (int i = 0; i < resourcesCount; i++)
        {
            Resource resource = (Resource)packet.ReadInt();
            int count = packet.ReadInt();
            if (resources.ContainsKey(resource))
                continue;
            resources.Add(resource, count);
        }

        Game.instance.RGBuildingResourcesChange(tileX, tileY, resources);

        Debug.Log($"RGBuilding's on tile(X:{tileX}, Y:{tileY}) resources changed.");
    }

    internal static void MovableTargetTileSet(Packet packet)
    {
        int playerId = packet.ReadInt();
        int movableId = packet.ReadInt();
        int tileX = packet.ReadInt();
        int tileY = packet.ReadInt();

        Game.instance.SetMovableTargetTile(movableId, tileX, tileY);

        Debug.Log($"Movable(ID:{movableId}) is now moving to tile(X:{tileX},Y:{tileY}).");
    }

    internal static void MovableMoved(Packet packet)
    {
        int movableId = packet.ReadInt();
        int tileX = packet.ReadInt();
        int tileY = packet.ReadInt();

        Game.instance.MovableMoved(movableId, tileX, tileY);

        // Debug.Log($"Movable(ID:{movableId}) has moved to tile(X:{tileX},Y:{tileY}).");
    }

    internal static void SpawnWarrior(Packet packet)
    {
        int playerId = packet.ReadInt();
        int movableId = packet.ReadInt();
        int warriorStrength = packet.ReadInt();

        Game.instance.SpawnWarrior(playerId, movableId, warriorStrength);

        Debug.Log($"Player(ID:{playerId}) has spawned a warrior in a movable(ID:{movableId}).");
    }

    internal static void MovableInventoryChanged(Packet packet)
    {
        int movableId = packet.ReadInt();

        int resourcesCount = packet.ReadInt();
        Dictionary<Resource, int> resources = new Dictionary<Resource, int>();
        for (int i = 0; i < resourcesCount; i++)
        {
            Resource resource = (Resource)packet.ReadInt();
            int count = packet.ReadInt();
            if (resources.ContainsKey(resource))
                return;
            resources.Add(resource, count);
        }

        Game.instance.UpdateMovableInventory(movableId, resources);
        Console.WriteLine($"Movable's(ID:{movableId}) inventory changed.");
    }
}

