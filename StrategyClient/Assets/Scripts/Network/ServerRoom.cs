﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerRoom : MonoBehaviour
{
    public int ID { get; private set; } = 0;
    public int PlayersCount { get; private set; } = 0;

    public ServerRoom(int id, int playersCount)
    {
        ID = id;
        PlayersCount = playersCount;
    }

}
