﻿using System.Collections.Generic;

public class ClientSend
{
    private static void SendTCPData(Packet packet)
    {
        packet.WriteLength();
        Client.instance.tcp.SendData(packet);
    }

    #region Packets
    public static void WelcomeReceived(string username)
    {
        using (Packet packet = new Packet((int)ClientPackets.welcomeReceived))
        {
            packet.Write(Client.instance.id);
            packet.Write(username);

            SendTCPData(packet);
        }
    }

    public static void RoomsStateRequest()
    {
        using (Packet packet = new Packet((int)ClientPackets.roomsStateRequest))
        {
            SendTCPData(packet);
        }
    }

    public static void MakeNewRoomRequest()
    {
        using (Packet packet = new Packet((int)ClientPackets.createNewRoomRequest))
        {
            SendTCPData(packet);
        }
    }

    public static void ConnectToRoomRequest(int roomId)
    {
        using (Packet packet = new Packet((int)ClientPackets.playerJoinRoomRequest))
        {
            packet.Write(Client.instance.id);
            packet.Write(roomId);

            SendTCPData(packet);
        }
    }

    public static void LeaveRoomRequest()
    {
        using (Packet packet = new Packet((int)ClientPackets.playerLeaveRoomRequest))
        {
            packet.Write(Client.instance.id);

            SendTCPData(packet);
        }
    }

    public static void ChangeReadyStateRequest()
    {
        using (Packet packet = new Packet((int)ClientPackets.playerReadyStateChangeRequest))
        {
            packet.Write(Client.instance.id);

            SendTCPData(packet);
        }
    }

    public static void BuildVillageRequest(Tile tile)
    {
        using (Packet packet = new Packet((int)ClientPackets.buildVillageRequest))
        {
            packet.Write(Client.instance.id);
            packet.Write(tile.Position.x);
            packet.Write(tile.Position.y);

            SendTCPData(packet);
        }
    }

    public static void BuildRoadRequest(Tile roadTile)
    {
        using (Packet packet = new Packet((int)ClientPackets.buildRoadRequest))
        {
            packet.Write(Client.instance.id);
            packet.Write(roadTile.Position.x);
            packet.Write(roadTile.Position.y);
            // packet.Write(fromTile.Position.x);
            // packet.Write(fromTile.Position.y);
            // packet.Write(toTile.Position.x);
            // packet.Write(toTile.Position.y);

            SendTCPData(packet);
        }
    }

    public static void BuildCaravanRequest(Tile tile)
    {
        using (Packet packet = new Packet((int)ClientPackets.buildCaravanRequest))
        {
            packet.Write(Client.instance.id);
            packet.Write(tile.Position.x);
            packet.Write(tile.Position.y);

            SendTCPData(packet);
        }
    }

    public static void SetBuildingGatheringTileRequest(ResourceGathering resourceGathering, Tile tile)
    {
        using (Packet packet = new Packet((int)ClientPackets.setBuildingGatheringTileRequest))
        {
            packet.Write(Client.instance.id);
            packet.Write(resourceGathering.Tile.Position.x);
            packet.Write(resourceGathering.Tile.Position.y);
            packet.Write(tile.Position.x);
            packet.Write(tile.Position.y);

            SendTCPData(packet);
        }
    }

    public static void SetMovableTargetTileRequest(Movable movable, Tile tile)
    {
        using (Packet packet = new Packet((int)ClientPackets.setMovableTargetTileRequest))
        {
            packet.Write(Client.instance.id);
            packet.Write(movable.MovableId);
            packet.Write(tile.Position.x);
            packet.Write(tile.Position.y);

            SendTCPData(packet);
        }
    }

    internal static void SpawnWarriorRequest(Movable movable, int warriorStrength)
    {
        using (Packet packet = new Packet((int)ClientPackets.spawnWarriorRequest))
        {
            packet.Write(Client.instance.id);
            packet.Write(movable.MovableId);
            packet.Write(warriorStrength);

            SendTCPData(packet);
        }
    }

    internal static void LoadResourcesToMovable(Movable movable)
    {
        if (movable.CurrentTile.Building == null
            || !(movable.CurrentTile.Building is ResourceGathering rgBuilding))
            return;
        
        LoadResourcesToMovable(movable, rgBuilding.Resources);
    }
    internal static void LoadResourcesToMovable(Movable movable, Dictionary<Resource, int> resources)
    {
        using (Packet packet = new Packet((int)ClientPackets.loadResourcesToMovableRequest))
        {
            packet.Write(Client.instance.id);
            packet.Write(movable.MovableId);

            packet.Write(resources.Count);
            foreach (KeyValuePair<Resource, int> keyValuePair in resources)
            {
                packet.Write((int)keyValuePair.Key);
                packet.Write(keyValuePair.Value);
            }

            SendTCPData(packet);
        }
    }

    internal static void UnloadResourcesFromMovable(Movable movable) => UnloadResourcesFromMovable(movable, movable.Resources);
    internal static void UnloadResourcesFromMovable(Movable movable, Dictionary<Resource, int> resources)
    {
        using (Packet packet = new Packet((int)ClientPackets.unloadResourcesFromMovableRequest))
        {
            packet.Write(Client.instance.id);
            packet.Write(movable.MovableId);

            packet.Write(resources.Count);
            foreach (KeyValuePair<Resource, int> keyValuePair in resources)
            {
                packet.Write((int)keyValuePair.Key);
                packet.Write(keyValuePair.Value);
            }

            SendTCPData(packet);
        }
    }
    #endregion
}

