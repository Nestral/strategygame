﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomPlayer : MonoBehaviour
{

    public Sprite readySprite;
    public Sprite notReadySprite;

    public Text playerNameText;
    public Image playerReadyImage;

    public void SetPlayerReady(bool ready)
    {
        playerReadyImage.sprite = ready ? readySprite : notReadySprite;
    }

    public void SetPlayerName(string name)
    {
        playerNameText.text = name;
    }

}
