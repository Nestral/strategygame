﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    #region Singleton
    public static UIManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.LogWarning("Instance already exists, destroying object!");
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        activeMenu = connectToServerMenu;
    }
    #endregion

    public Color[] playerColors;

    // Player resources
    public Text woodText;
    public Text clayText;
    public Text stoneText;
    public Text wheatText;

    // Room/Server UI
    public Sprite readySprite;
    public Sprite notReadySprite;
    public Image readyImage;

    public GameObject roomPlayerPrefab;

    public Text usernameText;
    public Text serverAddressText;
    public Text roomIdText;

    // Menues
    public GameObject connectToServerMenu;
    public GameObject connectToRoomMenu;
    public GameObject roomMenu;
    public GameObject roomPlayersHolder;
    public GameObject gameMenu;

    private GameObject activeMenu;

    // UI elements
    public GameObject tileUI;
    public GameObject buildingUI;
    public GameObject movableUI;

    public ResourcesDisplay buildingUIResourcesDisplay;
    public ResourcesDisplay movableUIResourcesDisplay;

    private Dictionary<int, RoomPlayer> roomPlayers = new Dictionary<int, RoomPlayer>();

    public string GetServerAddressString() => serverAddressText.text;

    public string GetRoomIdString() => roomIdText.text;

    public string GetUsername() => usernameText.text;

    internal void OnConnectedToServer()
    {
        SetActiveMenu(connectToRoomMenu);
    }

    internal void OnDisconnectedFromServer()
    {
        SetActiveMenu(connectToServerMenu);
    }

    internal void OnHostGame()
    {
        SetActiveMenu(roomMenu);
    }

    internal void OnConnectedToRoom(RoomManager room)
    {
        SetActiveMenu(roomMenu);

        foreach (Player player in room.GetPlayers())
        {
            if (player.ID != Client.instance.id)
            {
                RoomPlayer newPlayer = Instantiate(roomPlayerPrefab, roomPlayersHolder.transform).GetComponent<RoomPlayer>();
                newPlayer.SetPlayerName(player.Username);
                newPlayer.SetPlayerReady(player.Ready);
                roomPlayers.Add(player.ID, newPlayer);
            }
        }
    }

    internal void OnPlayerConnectedToRoom(int playerId, string playerName)
    {
        RoomPlayer newPlayer = Instantiate(roomPlayerPrefab, roomPlayersHolder.transform).GetComponent<RoomPlayer>();
        newPlayer.SetPlayerName(playerName);
        newPlayer.SetPlayerReady(false);
        roomPlayers.Add(playerId, newPlayer);
    }

    internal void OnPlayerDisconnectedFromRoom(int playerId)
    {
        if (playerId == Client.instance.id)
        {
            foreach (RoomPlayer player in roomPlayers.Values)
            {
                Destroy(player.gameObject);
            }
            roomPlayers.Clear();

            readyImage.sprite = notReadySprite;
            SetActiveMenu(connectToRoomMenu);
        }
        else
        {
            Destroy(roomPlayers[playerId].gameObject);
            roomPlayers.Remove(playerId);
        }
    }

    internal void OnGameStarted()
    {
        SetActiveMenu(gameMenu);
    }

    internal void OnGameEnded()
    {
        SetActiveMenu(roomMenu);
    }

    internal void OnPlayerReady(int playerId, bool ready)
    {
        if (playerId == Client.instance.id)
            readyImage.sprite = ready ? readySprite : notReadySprite;
        else
            roomPlayers[playerId].SetPlayerReady(ready);
    }

    internal void OnResourcesChange(Resource resource, int value)
    {
        switch (resource)
        {
            case Resource.None:
                break;
            case Resource.Wood:
                woodText.text = value.ToString();
                break;
            case Resource.Clay:
                clayText.text = value.ToString();
                break;
            case Resource.Stone:
                stoneText.text = value.ToString();
                break;
            case Resource.Wheat:
                wheatText.text = value.ToString();
                break;
            default:
                break;
        }
    }

    internal void SetUIResources(ResourcesDisplay display, Resource resource, int count) => display.SetResourceCount(resource, count);
    internal void SetUIResources(ResourcesDisplay display, Dictionary<Resource, int> resources) => display.SetResourceCount(resources);

    private void SetActiveMenu(GameObject menu)
    {
        activeMenu?.SetActive(false);
        menu?.SetActive(true);
        activeMenu = menu;
    }

    internal void EnableUI(GameObject ui)
    {
        ui?.SetActive(true);
    }

    internal void DisableUI(GameObject ui)
    {
        ui?.SetActive(false);
    }
}
