﻿using System.Collections.Generic;
using System.Linq;

public class RoomManager
{
    private Game game = Game.instance;

    public bool IsGameStarted { get; private set; } = false;

    public int Id { get; private set; } = 0;

    public static readonly int minPlayers = 2;
    public static readonly int maxPlayers = 10;

    private Dictionary<int, Player> players = new Dictionary<int, Player>();

    public RoomManager(int roomId)
    {
        Id = roomId;
    }

    internal Player[] GetPlayers() => players.Values.ToArray();

    internal void SetPlayers(Dictionary<int, Player> players)
    {
        this.players = players;
    }

    internal bool TryGetPlayer(int playerId, out Player player)
    {
        return players.TryGetValue(playerId, out player);
    }

    internal Player PlayerJoin(int playerId, string username)
    {
        Player newPlayer = new Player(playerId, username, UIManager.instance.playerColors[players.Count]);
        players.Add(playerId, newPlayer);

        return newPlayer;
    }

    internal void PlayerLeave(int playerId)
    {
        players.Remove(playerId);
    }

    internal void PlayerReady(int playerId, bool ready)
    {
        players[playerId].SetReady(ready);
    }

    internal void ResetGame()
    {
        IsGameStarted = false;
    }

    internal void StartGame()
    {
        IsGameStarted = true;
        game.StartGame(players);
    }

    internal void EndGame()
    {
        IsGameStarted = false;
        game.EndGame();
    }

}
